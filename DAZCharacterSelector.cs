using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using GPUTools.Physics.Scripts.Behaviours;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000A80 RID: 2688
[ExecuteInEditMode]
public class DAZCharacterSelector : JSONStorable
{
	// Token: 0x060041F3 RID: 16883 RVA: 0x0012EBCC File Offset: 0x0012CDCC
	public override JSONClass GetJSON(bool includePhysical = true, bool includeAppearance = true, bool forceStore = false)
	{
		JSONClass json = base.GetJSON(includePhysical, includeAppearance, forceStore);
		JSONArray jsonarray = new JSONArray();
		if (includeAppearance || forceStore)
		{
			this.needsStore = true;
			json["character"] = this.selectedCharacter.displayName;
			json["clothing"] = jsonarray;
			for (int i = 0; i < this.clothingItems.Length; i++)
			{
				DAZClothingItem dazclothingItem = this.clothingItems[i];
				if (dazclothingItem.active)
				{
					JSONClass jsonclass = new JSONClass();
					jsonarray.Add(jsonclass);
					jsonclass["id"] = dazclothingItem.uid;
					jsonclass["enabled"].AsBool = dazclothingItem.active;
				}
			}
			if (this.selectedHairGroup != null)
			{
				json["hair"] = this.selectedHairGroup.displayName;
			}
		}
		jsonarray = new JSONArray();
		json["morphs"] = jsonarray;
		if (this.morphsControlUI != null)
		{
			List<string> enabledMorphDisplayNames = this.morphsControlUI.GetEnabledMorphDisplayNames();
			if (enabledMorphDisplayNames != null)
			{
				foreach (string morphDisplayName in enabledMorphDisplayNames)
				{
					DAZMorph morphByDisplayName = this.morphsControlUI.GetMorphByDisplayName(morphDisplayName);
					if (morphByDisplayName != null)
					{
						bool isPoseControl = morphByDisplayName.isPoseControl;
						if ((includePhysical && isPoseControl) || (includeAppearance && !isPoseControl))
						{
							JSONClass jsonclass2 = new JSONClass();
							if (morphByDisplayName.StoreJSON(jsonclass2, false))
							{
								if (morphByDisplayName.isRuntime && SuperController.singleton != null && SuperController.singleton.packageMode)
								{
									string text = morphByDisplayName.metaLoadPath;
									text = Regex.Replace(text, ".*/Import/", "Import/");
									SuperController.singleton.AddFileToPackage(morphByDisplayName.metaLoadPath, text);
									text = morphByDisplayName.deltasLoadPath;
									text = Regex.Replace(text, ".*/Import/", "Import/");
									SuperController.singleton.AddFileToPackage(morphByDisplayName.deltasLoadPath, text);
								}
								this.needsStore = true;
								jsonarray.Add(jsonclass2);
							}
						}
					}
				}
			}
			else
			{
				Debug.LogWarning("morphDisplayNames not set for " + base.name + " character " + this.selectedCharacter.displayName);
			}
		}
		else
		{
			Debug.LogWarning("morphsControl UI not set for " + base.name + " character " + this.selectedCharacter.displayName);
		}
		return json;
	}

	// Token: 0x060041F4 RID: 16884 RVA: 0x0012EE80 File Offset: 0x0012D080
	private void ResetClothing(bool clearAll = false)
	{
		this.Init(false);
		foreach (DAZClothingItem dazclothingItem in this._femaleClothingItems)
		{
			if (clearAll)
			{
				this.SetActiveClothingItem(dazclothingItem, false, false);
			}
			else
			{
				this.SetActiveClothingItem(dazclothingItem, dazclothingItem.startActive, false);
			}
		}
		foreach (DAZClothingItem dazclothingItem2 in this._maleClothingItems)
		{
			if (clearAll)
			{
				this.SetActiveClothingItem(dazclothingItem2, false, false);
			}
			else
			{
				this.SetActiveClothingItem(dazclothingItem2, dazclothingItem2.startActive, false);
			}
		}
	}

	// Token: 0x060041F5 RID: 16885 RVA: 0x0012EF20 File Offset: 0x0012D120
	public void ResetMorphs(bool resetPhysical, bool resetAppearance)
	{
		this.Init(false);
		if (this.morphsControlUI != null)
		{
			List<string> morphDisplayNames = this.morphsControlUI.GetMorphDisplayNames();
			if (morphDisplayNames != null)
			{
				foreach (string morphDisplayName in morphDisplayNames)
				{
					DAZMorph morphByDisplayName = this.morphsControlUI.GetMorphByDisplayName(morphDisplayName);
					if (morphByDisplayName != null)
					{
						bool isPoseControl = morphByDisplayName.isPoseControl;
						if ((resetPhysical && isPoseControl) || (resetAppearance && !isPoseControl))
						{
							morphByDisplayName.Reset();
							morphByDisplayName.SetAnimatable(false);
						}
					}
				}
			}
		}
		if (this.morphBank1 != null)
		{
			this.morphBank1.ResetMorphs();
		}
		if (this.morphBank2 != null)
		{
			this.morphBank2.ResetMorphs();
		}
		if (this.morphBank3 != null)
		{
			this.morphBank3.ResetMorphs();
		}
		if (this._characterRun != null)
		{
			this._characterRun.ResetMorphs();
		}
	}

	// Token: 0x060041F6 RID: 16886 RVA: 0x0012F04C File Offset: 0x0012D24C
	public override void RestoreFromJSON(JSONClass jc, bool restorePhysical = true, bool restoreAppearance = true, JSONArray presetAtoms = null, bool setMissingToDefault = true)
	{
		this.Init(false);
		base.RestoreFromJSON(jc, restorePhysical, restoreAppearance, presetAtoms, setMissingToDefault);
		if (restoreAppearance)
		{
			if (jc["character"] != null)
			{
				string text = jc["character"];
				if (text == string.Empty)
				{
					this.SelectCharacterByName(this.startingCharacter.displayName, true);
				}
				else
				{
					this.SelectCharacterByName(text, true);
				}
			}
			else if (setMissingToDefault)
			{
				this.SelectCharacterByName(this.startingCharacter.displayName, true);
			}
			if (jc["hair"] != null)
			{
				string text2 = jc["hair"];
				if (text2 == string.Empty)
				{
					this.SetSelectedHairGroup(this.startingHairGroup.displayName, true);
				}
				else
				{
					this.SetSelectedHairGroup(text2, true);
				}
			}
			else if (setMissingToDefault)
			{
				this.SetSelectedHairGroup(this.startingHairGroup.displayName, true);
			}
			if (jc["clothing"] != null)
			{
				JSONArray asArray = jc["clothing"].AsArray;
				if (asArray != null)
				{
					this.ResetClothing(true);
					IEnumerator enumerator = asArray.GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							object obj = enumerator.Current;
							JSONClass jsonclass = (JSONClass)obj;
							string text3 = jsonclass["id"];
							if (text3 == null)
							{
								text3 = jsonclass["name"];
							}
							bool asBool = jsonclass["enabled"].AsBool;
							this.SetActiveClothingItem(text3, asBool, true);
						}
						goto IL_1AD;
					}
					finally
					{
						IDisposable disposable;
						if ((disposable = (enumerator as IDisposable)) != null)
						{
							disposable.Dispose();
						}
					}
				}
				this.ResetClothing(false);
			}
			else if (setMissingToDefault)
			{
				this.ResetClothing(false);
			}
		}
		IL_1AD:
		if (jc["morphs"] != null && this.morphsControlUI != null)
		{
			this.ResetMorphs(restorePhysical, restoreAppearance);
			if (restoreAppearance && SuperController.singleton != null)
			{
				bool flag = false;
				if (this.morphBank1 != null && this.morphBank1.LoadTransientMorphs(SuperController.singleton.currentLoadDir))
				{
					flag = true;
				}
				if (this.morphBank2 != null && this.morphBank2.LoadTransientMorphs(SuperController.singleton.currentLoadDir))
				{
					flag = true;
				}
				if (this.morphBank3 != null && this.morphBank3.LoadTransientMorphs(SuperController.singleton.currentLoadDir))
				{
					flag = true;
				}
				if (flag)
				{
					if (this.morphsControlUI != null)
					{
						this.morphsControlUI.ResyncMorphs();
					}
					if (this.morphsControlUIAlt != null)
					{
						this.morphsControlUIAlt.ResyncMorphs();
					}
				}
			}
			JSONArray asArray2 = jc["morphs"].AsArray;
			if (!(asArray2 != null))
			{
				goto IL_3AB;
			}
			IEnumerator enumerator2 = asArray2.GetEnumerator();
			try
			{
				while (enumerator2.MoveNext())
				{
					object obj2 = enumerator2.Current;
					JSONClass jsonclass2 = (JSONClass)obj2;
					string text4 = jsonclass2["name"];
					if (text4 != null)
					{
						DAZMorph morphByDisplayName = this.morphsControlUI.GetMorphByDisplayName(text4);
						if (morphByDisplayName != null)
						{
							bool isPoseControl = morphByDisplayName.isPoseControl;
							if ((restorePhysical && isPoseControl) || (restoreAppearance && !isPoseControl))
							{
								morphByDisplayName.RestoreFromJSON(jsonclass2);
							}
						}
						else
						{
							SuperController.LogError("Could not find morph " + text4 + " referenced in save file", true);
							DAZMorph morph = this.morphsControlUI.GetMorph(text4);
							if (morph != null)
							{
							    UnityEngine.Debug.Log("Detected name==id. Morph loaded by id instead of displayName! :D");
								bool isPoseControl2 = morph.isPoseControl;
								if ((restorePhysical && isPoseControl2) || (restoreAppearance && !isPoseControl2))
								{
									morph.RestoreFromJSON(jsonclass2);
								}
							}
						}
					}
				}
				goto IL_3AB;
			}
			finally
			{
				IDisposable disposable2;
				if ((disposable2 = (enumerator2 as IDisposable)) != null)
				{
					disposable2.Dispose();
				}
			}
		}
		if (setMissingToDefault)
		{
			this.ResetMorphs(restorePhysical, restoreAppearance);
		}
		IL_3AB:
		if (this.morphBank1 != null)
		{
			this.morphBank1.ApplyMorphsImmediate();
		}
		if (this.morphBank2 != null)
		{
			this.morphBank2.ApplyMorphsImmediate();
		}
		if (this.morphBank3 != null)
		{
			this.morphBank3.ApplyMorphsImmediate();
		}
	}

	// Token: 0x170008AB RID: 2219
	// (get) Token: 0x060041F7 RID: 16887 RVA: 0x0012F46C File Offset: 0x0012D66C
	public DAZMorphBank morphBank1
	{
		get
		{
			DAZCharacterSelector.Gender gender = this.gender;
			if (gender == DAZCharacterSelector.Gender.Female)
			{
				return this.femaleMorphBank1;
			}
			if (gender != DAZCharacterSelector.Gender.Male)
			{
				return null;
			}
			return this.maleMorphBank1;
		}
	}

	// Token: 0x170008AC RID: 2220
	// (get) Token: 0x060041F8 RID: 16888 RVA: 0x0012F4A4 File Offset: 0x0012D6A4
	public DAZMorphBank morphBank2
	{
		get
		{
			DAZCharacterSelector.Gender gender = this.gender;
			if (gender == DAZCharacterSelector.Gender.Female)
			{
				return this.femaleMorphBank2;
			}
			if (gender != DAZCharacterSelector.Gender.Male)
			{
				return null;
			}
			return this.maleMorphBank2;
		}
	}

	// Token: 0x170008AD RID: 2221
	// (get) Token: 0x060041F9 RID: 16889 RVA: 0x0012F4DC File Offset: 0x0012D6DC
	public DAZMorphBank morphBank3
	{
		get
		{
			DAZCharacterSelector.Gender gender = this.gender;
			if (gender == DAZCharacterSelector.Gender.Female)
			{
				return this.femaleMorphBank3;
			}
			if (gender != DAZCharacterSelector.Gender.Male)
			{
				return null;
			}
			return this.maleMorphBank3;
		}
	}

	// Token: 0x170008AE RID: 2222
	// (get) Token: 0x060041FA RID: 16890 RVA: 0x0002B366 File Offset: 0x00029566
	// (set) Token: 0x060041FB RID: 16891 RVA: 0x0002B36E File Offset: 0x0002956E
	public DAZCharacterSelector.Gender gender
	{
		get
		{
			return this._gender;
		}
		set
		{
			if (this._gender != value)
			{
				this._gender = value;
				this.SyncGender();
			}
		}
	}

	// Token: 0x060041FC RID: 16892 RVA: 0x0012F514 File Offset: 0x0012D714
	protected void SyncGender()
	{
		foreach (Transform transform in this.maleTransforms)
		{
			if (transform != null)
			{
				if (this._gender == DAZCharacterSelector.Gender.Both || this._gender == DAZCharacterSelector.Gender.Male)
				{
					transform.gameObject.SetActive(true);
				}
				else
				{
					transform.gameObject.SetActive(false);
				}
			}
		}
		if (this.maleMorphBank1 != null)
		{
			if (this._gender == DAZCharacterSelector.Gender.Both || this._gender == DAZCharacterSelector.Gender.Male)
			{
				this.maleMorphBank1.gameObject.SetActive(true);
			}
			else
			{
				this.maleMorphBank1.gameObject.SetActive(false);
			}
		}
		if (this.maleMorphBank2 != null)
		{
			if (this._gender == DAZCharacterSelector.Gender.Both || this._gender == DAZCharacterSelector.Gender.Male)
			{
				this.maleMorphBank2.gameObject.SetActive(true);
			}
			else
			{
				this.maleMorphBank2.gameObject.SetActive(false);
			}
		}
		if (this.maleMorphBank3 != null)
		{
			if (this._gender == DAZCharacterSelector.Gender.Both || this._gender == DAZCharacterSelector.Gender.Male)
			{
				this.maleMorphBank3.gameObject.SetActive(true);
			}
			else
			{
				this.maleMorphBank3.gameObject.SetActive(false);
			}
		}
		foreach (Transform transform2 in this.femaleTransforms)
		{
			if (transform2 != null)
			{
				if (this._gender == DAZCharacterSelector.Gender.Both || this._gender == DAZCharacterSelector.Gender.Female)
				{
					transform2.gameObject.SetActive(true);
				}
				else
				{
					transform2.gameObject.SetActive(false);
				}
			}
		}
		if (this.femaleMorphBank1 != null)
		{
			if (this._gender == DAZCharacterSelector.Gender.Both || this._gender == DAZCharacterSelector.Gender.Female)
			{
				this.femaleMorphBank1.gameObject.SetActive(true);
			}
			else
			{
				this.femaleMorphBank1.gameObject.SetActive(false);
			}
		}
		if (this.femaleMorphBank2 != null)
		{
			if (this._gender == DAZCharacterSelector.Gender.Both || this._gender == DAZCharacterSelector.Gender.Female)
			{
				this.femaleMorphBank2.gameObject.SetActive(true);
			}
			else
			{
				this.femaleMorphBank2.gameObject.SetActive(false);
			}
		}
		if (this.femaleMorphBank3 != null)
		{
			if (this._gender == DAZCharacterSelector.Gender.Both || this._gender == DAZCharacterSelector.Gender.Female)
			{
				this.femaleMorphBank3.gameObject.SetActive(true);
			}
			else
			{
				this.femaleMorphBank3.gameObject.SetActive(false);
			}
		}
		if (this.rootBones != null)
		{
			if (this._gender == DAZCharacterSelector.Gender.Male)
			{
				this.rootBones.name = this.rootBonesNameMale;
				this.rootBones.isMale = true;
			}
			else if (this._gender == DAZCharacterSelector.Gender.Female)
			{
				this.rootBones.name = this.rootBonesNameFemale;
				this.rootBones.isMale = false;
			}
			else
			{
				this.rootBones.name = this.rootBonesName;
				this.rootBones.isMale = false;
			}
		}
		this.SyncColliders();
		this.Init(true);
	}

	// Token: 0x060041FD RID: 16893 RVA: 0x0002B389 File Offset: 0x00029589
	protected void SyncUseAuxBreastColliders(bool b)
	{
		this._useAuxBreastColliders = b;
		this.SyncColliders();
	}

	// Token: 0x170008AF RID: 2223
	// (get) Token: 0x060041FE RID: 16894 RVA: 0x0002B398 File Offset: 0x00029598
	// (set) Token: 0x060041FF RID: 16895 RVA: 0x0002B3A0 File Offset: 0x000295A0
	public bool useAuxBreastColliders
	{
		get
		{
			return this._useAuxBreastColliders;
		}
		set
		{
			if (this.useAuxBreastCollidersJSON != null)
			{
				this.useAuxBreastCollidersJSON.val = value;
			}
			else if (this._useAuxBreastColliders != value)
			{
				this.SyncUseAuxBreastColliders(value);
			}
		}
	}

	// Token: 0x06004200 RID: 16896 RVA: 0x0002B3D1 File Offset: 0x000295D1
	protected void SyncUseAdvancedColliders(bool b)
	{
		this._useAdvancedColliders = b;
		this.SyncColliders();
	}

	// Token: 0x170008B0 RID: 2224
	// (get) Token: 0x06004201 RID: 16897 RVA: 0x0002B3E0 File Offset: 0x000295E0
	// (set) Token: 0x06004202 RID: 16898 RVA: 0x0002B3E8 File Offset: 0x000295E8
	public bool useAdvancedColliders
	{
		get
		{
			return this._useAdvancedColliders;
		}
		set
		{
			if (this.useAdvancedCollidersJSON != null)
			{
				this.useAdvancedCollidersJSON.val = value;
			}
			else if (this._useAdvancedColliders != value)
			{
				this.SyncUseAdvancedColliders(value);
			}
		}
	}

	// Token: 0x06004203 RID: 16899 RVA: 0x0012F860 File Offset: 0x0012DA60
	protected void SyncColliders()
	{
		if (this._useAdvancedColliders)
		{
			foreach (Transform transform in this.regularCollidersFemale)
			{
				transform.gameObject.SetActive(false);
			}
			foreach (Transform transform2 in this.regularCollidersMale)
			{
				transform2.gameObject.SetActive(false);
			}
			foreach (Transform transform3 in this.regularColliders)
			{
				transform3.gameObject.SetActive(false);
			}
			if (this._gender == DAZCharacterSelector.Gender.Male)
			{
				foreach (Transform transform4 in this.advancedCollidersFemale)
				{
					transform4.gameObject.SetActive(false);
				}
				foreach (Transform transform5 in this.advancedCollidersMale)
				{
					transform5.gameObject.SetActive(true);
				}
			}
			else
			{
				foreach (Transform transform6 in this.advancedCollidersMale)
				{
					transform6.gameObject.SetActive(false);
				}
				foreach (Transform transform7 in this.advancedCollidersFemale)
				{
					transform7.gameObject.SetActive(true);
				}
			}
		}
		else
		{
			foreach (Transform transform8 in this.advancedCollidersFemale)
			{
				transform8.gameObject.SetActive(false);
			}
			foreach (Transform transform9 in this.advancedCollidersMale)
			{
				transform9.gameObject.SetActive(false);
			}
			if (this._gender == DAZCharacterSelector.Gender.Male)
			{
				foreach (Transform transform10 in this.regularCollidersFemale)
				{
					transform10.gameObject.SetActive(false);
				}
				foreach (Transform transform11 in this.regularCollidersMale)
				{
					transform11.gameObject.SetActive(true);
				}
			}
			else
			{
				foreach (Transform transform12 in this.regularCollidersMale)
				{
					transform12.gameObject.SetActive(false);
				}
				foreach (Transform transform13 in this.regularCollidersFemale)
				{
					transform13.gameObject.SetActive(true);
				}
			}
			foreach (Transform transform14 in this.regularColliders)
			{
				transform14.gameObject.SetActive(true);
			}
		}
		foreach (Collider collider in this.auxBreastColliders)
		{
			collider.enabled = this.useAuxBreastColliders;
			CapsuleLineSphereCollider component = collider.GetComponent<CapsuleLineSphereCollider>();
			if (component != null)
			{
				component.enabled = this.useAuxBreastColliders;
			}
			GpuSphereCollider component2 = collider.GetComponent<GpuSphereCollider>();
			if (component2 != null)
			{
				component2.enabled = this.useAuxBreastColliders;
			}
		}
		if (Application.isPlaying)
		{
			foreach (IgnoreChildColliders ignoreChildColliders2 in this._ignoreChildColliders)
			{
				ignoreChildColliders2.SyncColliders();
			}
			foreach (DAZPhysicsMesh dazphysicsMesh in this._physicsMeshes)
			{
				dazphysicsMesh.InitColliders();
			}
			foreach (AutoColliderBatchUpdater autoColliderBatchUpdater in this._autoColliderBatchUpdaters)
			{
				if (autoColliderBatchUpdater != null)
				{
					autoColliderBatchUpdater.UpdateAutoColliders();
				}
			}
			foreach (AutoColliderGroup autoColliderGroup in this._autoColliderGroups)
			{
				if (autoColliderGroup != null && autoColliderGroup.isActiveAndEnabled)
				{
					autoColliderGroup.InitColliders();
				}
			}
		}
	}

	// Token: 0x06004204 RID: 16900 RVA: 0x0002B419 File Offset: 0x00029619
	protected void SyncDisableAnatomy(bool b)
	{
		this._disableAnatomy = b;
		this.SyncAnatomy();
	}

	// Token: 0x06004205 RID: 16901 RVA: 0x0002B428 File Offset: 0x00029628
	public void InitBones()
	{
		if (this.rootBones != null)
		{
			this.rootBones.Init();
			this.maleAnatomyComponents = this.rootBones.GetComponentsInChildren<DAZMaleAnatomy>(true);
		}
	}

	// Token: 0x06004206 RID: 16902 RVA: 0x0002B458 File Offset: 0x00029658
	protected void SyncCharacterChoice(string choice)
	{
		this.SelectCharacterByName(choice, false);
	}

	// Token: 0x06004207 RID: 16903 RVA: 0x0012FCE0 File Offset: 0x0012DEE0
	public void InitCharacters()
	{
		if (this.characterChooserJSON != null)
		{
			base.DeregisterStringChooser(this.characterChooserJSON);
		}
		if (this.femaleCharactersContainer != null)
		{
			this._femaleCharacters = this.femaleCharactersContainer.GetComponentsInChildren<DAZCharacter>(true);
		}
		else
		{
			this._femaleCharacters = new DAZCharacter[0];
		}
		if (this.maleCharactersContainer != null)
		{
			this._maleCharacters = this.maleCharactersContainer.GetComponentsInChildren<DAZCharacter>(true);
		}
		else
		{
			this._maleCharacters = new DAZCharacter[0];
		}
		this._characterByName = new Dictionary<string, DAZCharacter>();
		this._characters = new DAZCharacter[this._femaleCharacters.Length + this._maleCharacters.Length];
		int num = 0;
		for (int i = 0; i < this._femaleCharacters.Length; i++)
		{
			this._characters[num] = this._femaleCharacters[i];
			num++;
		}
		for (int j = 0; j < this._maleCharacters.Length; j++)
		{
			this._characters[num] = this._maleCharacters[j];
			num++;
		}
		List<string> list = new List<string>();
		string startingValue = string.Empty;
		foreach (DAZCharacter dazcharacter in this._characters)
		{
			if (dazcharacter != null)
			{
				if (this._characterByName.ContainsKey(dazcharacter.displayName))
				{
					Debug.LogError("Character " + dazcharacter.displayName + " is a duplicate. Cannot add");
				}
				else
				{
					list.Add(dazcharacter.displayName);
					this._characterByName.Add(dazcharacter.displayName, dazcharacter);
					if (dazcharacter.gameObject.activeSelf)
					{
						startingValue = dazcharacter.displayName;
						this._selectedCharacter = dazcharacter;
					}
				}
			}
		}
		if (Application.isPlaying)
		{
			this.characterChooserJSON = new JSONStorableStringChooser("characterSelection", list, startingValue, "Character Selection", new JSONStorableStringChooser.SetStringCallback(this.SyncCharacterChoice));
			this.characterChooserJSON.isRestorable = false;
			this.characterChooserJSON.isStorable = false;
			base.RegisterStringChooser(this.characterChooserJSON);
		}
	}

	// Token: 0x170008B1 RID: 2225
	// (get) Token: 0x06004208 RID: 16904 RVA: 0x0002B462 File Offset: 0x00029662
	public DAZCharacter[] femaleCharacters
	{
		get
		{
			return this._femaleCharacters;
		}
	}

	// Token: 0x170008B2 RID: 2226
	// (get) Token: 0x06004209 RID: 16905 RVA: 0x0002B46A File Offset: 0x0002966A
	public DAZCharacter[] maleCharacters
	{
		get
		{
			return this._maleCharacters;
		}
	}

	// Token: 0x170008B3 RID: 2227
	// (get) Token: 0x0600420A RID: 16906 RVA: 0x0002B472 File Offset: 0x00029672
	public DAZCharacter[] characters
	{
		get
		{
			return this._characters;
		}
	}

	// Token: 0x0600420B RID: 16907 RVA: 0x0012FF00 File Offset: 0x0012E100
	public void SelectCharacterByName(string characterName, bool fromRestore = false)
	{
		if (this._characterByName == null)
		{
			this.Init(false);
		}
		DAZCharacter dazcharacter;
		if (this._characterByName.TryGetValue(characterName, out dazcharacter))
		{
			if (fromRestore)
			{
				dazcharacter.needsPostLoadJSONRestore = true;
			}
			this.selectedCharacter = dazcharacter;
		}
		if (this.characterChooserJSON != null)
		{
			this.characterChooserJSON.valNoCallback = characterName;
		}
	}

	// Token: 0x0600420C RID: 16908 RVA: 0x0012FF60 File Offset: 0x0012E160
	protected void ConnectSkin()
	{
		DAZSkinV2 skin = this._selectedCharacter.skin;
		DAZSkinV2 skinForClothes = this._selectedCharacter.skinForClothes;
		if (skin != null)
		{
			skin.Init();
			skin.ResetPostSkinMorphs();
			if (this._setDAZMorphs != null)
			{
				foreach (SetDAZMorph setDAZMorph in this._setDAZMorphs)
				{
					if (this.morphBank1 != null)
					{
						setDAZMorph.morphBank = this.morphBank1;
					}
					if (this.morphBank2 != null)
					{
						setDAZMorph.morphBankAlt = this.morphBank2;
					}
					if (this.morphBank3 != null)
					{
						setDAZMorph.morphBankAlt2 = this.morphBank3;
					}
				}
			}
			if (this._characterRun != null)
			{
				this._characterRun.morphBank1 = this.morphBank1;
				this._characterRun.morphBank2 = this.morphBank2;
				this._characterRun.morphBank3 = this.morphBank3;
				this._characterRun.skin = (DAZMergedSkinV2)skin;
				this._characterRun.bones = this.rootBones;
				this._characterRun.autoColliderUpdaters = this._autoColliderBatchUpdaters;
				this._characterRun.Connect();
			}
			if (this._eyelidControl != null)
			{
				this._eyelidControl.morphBank = this.morphBank1;
			}
			if (this._physicsMeshes != null)
			{
				foreach (DAZPhysicsMesh dazphysicsMesh in this._physicsMeshes)
				{
					if (dazphysicsMesh != null && dazphysicsMesh.isEnabled)
					{
						dazphysicsMesh.skinTransform = skin.transform;
						dazphysicsMesh.skin = skin;
					}
				}
			}
			if (this._autoColliderBatchUpdaters != null)
			{
				foreach (AutoColliderBatchUpdater autoColliderBatchUpdater in this._autoColliderBatchUpdaters)
				{
					autoColliderBatchUpdater.skin = skin;
				}
			}
			if (this._autoColliders != null)
			{
				foreach (AutoCollider autoCollider in this._autoColliders)
				{
					if (autoCollider != null)
					{
						autoCollider.skinTransform = skin.transform;
						autoCollider.skin = skin;
					}
				}
			}
			if (this._setAnchorFromVertexComps != null)
			{
				foreach (SetAnchorFromVertex setAnchorFromVertex in this._setAnchorFromVertexComps)
				{
					if (setAnchorFromVertex != null)
					{
						setAnchorFromVertex.skinTransform = skin.transform;
						setAnchorFromVertex.skin = skin;
					}
				}
			}
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				if (this.maleEyelashMaterialOptions != null)
				{
					this.maleEyelashMaterialOptions.skin = skin;
				}
			}
			else if (this.femaleEyelashMaterialOptions != null)
			{
				this.femaleEyelashMaterialOptions.skin = skin;
			}
			foreach (DAZCharacterMaterialOptions dazcharacterMaterialOptions in this._materialOptions)
			{
				if (dazcharacterMaterialOptions != null)
				{
					dazcharacterMaterialOptions.skin = skin;
					if (dazcharacterMaterialOptions.isPassthrough)
					{
						dazcharacterMaterialOptions.ConnectPassthroughBuckets();
					}
				}
			}
			this.ConnectCharacterMaterialOptionsUI();
			foreach (DAZClothingItem dazclothingItem in this.clothingItems)
			{
				if (dazclothingItem != null)
				{
					dazclothingItem.skin = skinForClothes;
				}
			}
			foreach (DAZHairGroup dazhairGroup in this.hairGroups)
			{
				if (dazhairGroup != null)
				{
					dazhairGroup.skin = skin;
				}
			}
		}
	}

	// Token: 0x0600420D RID: 16909 RVA: 0x00130328 File Offset: 0x0012E528
	private IEnumerator DelayResume(AsyncFlag af, int count)
	{
		this.delayResumeFlag = af;
		for (int i = 0; i < count; i++)
		{
			yield return null;
		}
		af.flag = true;
		yield break;
	}

	// Token: 0x0600420E RID: 16910 RVA: 0x00130354 File Offset: 0x0012E554
	protected void OnCharacterLoaded()
	{
		DAZMesh[] componentsInChildren = this._selectedCharacter.GetComponentsInChildren<DAZMesh>(true);
		foreach (DAZMesh dazmesh in componentsInChildren)
		{
			if (this.morphBank1 != null && this.morphBank1.geometryId == dazmesh.geometryId)
			{
				this.morphBank1.connectedMesh = dazmesh;
				dazmesh.morphBank = this.morphBank1;
			}
			if (this.morphBank2 != null && this.morphBank2.geometryId == dazmesh.geometryId)
			{
				this.morphBank2.connectedMesh = dazmesh;
				dazmesh.morphBank = this.morphBank2;
			}
			if (this.morphBank3 != null && this.morphBank3.geometryId == dazmesh.geometryId)
			{
				this.morphBank3.connectedMesh = dazmesh;
				dazmesh.morphBank = this.morphBank3;
			}
		}
		this.ConnectSkin();
		this.SyncAnatomy();
		if (this.onCharacterLoadedFlag != null)
		{
			base.StartCoroutine(this.DelayResume(this.onCharacterLoadedFlag, 10));
			this.onCharacterLoadedFlag = null;
		}
	}

	// Token: 0x170008B4 RID: 2228
	// (get) Token: 0x0600420F RID: 16911 RVA: 0x0002B47A File Offset: 0x0002967A
	// (set) Token: 0x06004210 RID: 16912 RVA: 0x00130488 File Offset: 0x0012E688
	public DAZCharacter selectedCharacter
	{
		get
		{
			return this._selectedCharacter;
		}
		set
		{
			if (this._selectedCharacter != value)
			{
				if (this._selectedCharacter != null)
				{
					DAZCharacter selectedCharacter = this._selectedCharacter;
					selectedCharacter.onLoadedHandlers = (JSONStorableDynamic.OnLoaded)Delegate.Remove(selectedCharacter.onLoadedHandlers, new JSONStorableDynamic.OnLoaded(this.OnCharacterLoaded));
					this._selectedCharacter.gameObject.SetActive(false);
					this.DisconnectCharacterOptionsUI();
				}
				this._selectedCharacter = value;
				if (this._selectedCharacter != null)
				{
					if (this._selectedCharacter.isMale)
					{
						if (this._gender != DAZCharacterSelector.Gender.Male)
						{
							this.gender = DAZCharacterSelector.Gender.Male;
						}
					}
					else if (this._gender != DAZCharacterSelector.Gender.Female)
					{
						this.gender = DAZCharacterSelector.Gender.Female;
					}
					if (this.characterSelectorUI != null)
					{
						this.characterSelectorUI.SetActiveCharacterToggleNoCallback(this._selectedCharacter.displayName);
					}
					if (this.characterSelectorUIAlt != null)
					{
						this.characterSelectorUIAlt.SetActiveCharacterToggleNoCallback(this._selectedCharacter.displayName);
					}
					if (Application.isPlaying)
					{
						if (this.onCharacterLoadedFlag != null && !this.onCharacterLoadedFlag.flag)
						{
							Debug.LogWarning("onCharacterLoadedFlag still set while trying to load another. Set to " + this.onCharacterLoadedFlag.name);
							this.onCharacterLoadedFlag.flag = true;
						}
						this.onCharacterLoadedFlag = new AsyncFlag();
						this.onCharacterLoadedFlag.name = "Character load: " + this._selectedCharacter.displayName;
						if (SuperController.singleton != null && (this.containingAtom == null || this.containingAtom.on))
						{
							SuperController.singleton.PauseSimulation(this.onCharacterLoadedFlag);
						}
					}
					DAZCharacter selectedCharacter2 = this._selectedCharacter;
					selectedCharacter2.onLoadedHandlers = (JSONStorableDynamic.OnLoaded)Delegate.Combine(selectedCharacter2.onLoadedHandlers, new JSONStorableDynamic.OnLoaded(this.OnCharacterLoaded));
					this._selectedCharacter.gameObject.SetActive(true);
				}
			}
			else if (this._selectedCharacter.needsPostLoadJSONRestore)
			{
				this._selectedCharacter.gameObject.SetActive(false);
				this._selectedCharacter.gameObject.SetActive(true);
			}
		}
	}

	// Token: 0x06004211 RID: 16913 RVA: 0x001306BC File Offset: 0x0012E8BC
	protected void SyncClothingItem(JSONStorableBool clothingItemJSON)
	{
		string altName = clothingItemJSON.altName;
		this.SetActiveClothingItem(altName, clothingItemJSON.val, false);
	}

	// Token: 0x06004212 RID: 16914 RVA: 0x001306E0 File Offset: 0x0012E8E0
	protected void FindFiles(string dir, string pattern, List<string> foundFiles)
	{
		if (Directory.Exists(dir))
		{
			foreach (string text in Directory.GetDirectories(dir))
			{
				foreach (string text2 in Directory.GetFiles(text, pattern))
				{
					string item = text2.Replace("\\", "/");
					foundFiles.Add(item);
				}
				this.FindFiles(text, pattern, foundFiles);
			}
		}
	}

	// Token: 0x06004213 RID: 16915 RVA: 0x00130764 File Offset: 0x0012E964
	protected void RefreshClothingItemThumbnail(DAZClothingItem dci)
	{
		if (ImageLoaderThreaded.singleton != null && dci.dynamicRuntimeLoadPath != null && dci.dynamicRuntimeLoadPath != string.Empty)
		{
			string text = dci.dynamicRuntimeLoadPath.Replace(".vam", ".jpg");
			if (File.Exists(text))
			{
				ImageLoaderThreaded.QueuedImage qi = new ImageLoaderThreaded.QueuedImage();
				qi.imgPath = text;
				qi.width = 512;
				qi.height = 512;
				qi.setSize = true;
				qi.callback = delegate(ImageLoaderThreaded.QueuedImage A_1)
				{
					dci.thumbnail = qi.tex;
					if (this.clothingSelectorFemaleUICustom != null)
					{
						this.clothingSelectorFemaleUICustom.RefreshThumbnails();
					}
				};
				ImageLoaderThreaded.singleton.QueueThumbnail(qi);
			}
		}
	}

	// Token: 0x06004214 RID: 16916 RVA: 0x0013085C File Offset: 0x0012EA5C
	protected void SyncCustomClothingItems(DAZClothingItem.Gender gender, DAZClothingItem[] existingItems, string searchPath)
	{
		Dictionary<string, DAZClothingItem> dictionary = new Dictionary<string, DAZClothingItem>();
		foreach (DAZClothingItem dazclothingItem in existingItems)
		{
			if (!dictionary.ContainsKey(dazclothingItem.uid))
			{
				dictionary.Add(dazclothingItem.uid, dazclothingItem);
			}
			if (dazclothingItem.clothingType == DAZClothingItem.ClothingType.Custom)
			{
				this.RefreshClothingItemThumbnail(dazclothingItem);
			}
		}
		List<string> list = new List<string>();
		this.FindFiles(searchPath, "*.vam", list);
		foreach (string text in list)
		{
			try
			{
				using (StreamReader streamReader = new StreamReader(text))
				{
					string aJSON = streamReader.ReadToEnd();
					JSONClass asObject = JSON.Parse(aJSON).AsObject;
					string text2 = string.Empty;
					if (asObject["uid"] != null)
					{
						text2 = asObject["uid"];
					}
					string text3 = asObject["displayName"];
					if (text3 == null || text3 == string.Empty)
					{
						text3 = text2;
					}
					if (text2 != string.Empty && text3 != string.Empty && !dictionary.ContainsKey(text2))
					{
						Transform transform = UnityEngine.Object.Instantiate<Transform>(this.dynamicClothingItemPrefab);
						transform.name = transform.name.Replace("(Clone)", string.Empty);
						transform.gameObject.SetActive(false);
						if (gender == DAZClothingItem.Gender.Female)
						{
							transform.parent = this.femaleClothingContainer;
						}
						else
						{
							transform.parent = this.maleClothingContainer;
						}
						transform.localPosition = Vector3.zero;
						transform.localRotation = Quaternion.identity;
						transform.localScale = Vector3.one;
						DAZClothingItem component = transform.GetComponent<DAZClothingItem>();
						if (component != null)
						{
							component.containingAtom = this.containingAtom;
							component.UIbucket = this.customUIBucket;
							component.gender = gender;
							component.uid = text2;
							component.displayName = text3;
							if (this._selectedCharacter != null)
							{
								component.skin = this._selectedCharacter.skin;
							}
							component.dynamicRuntimeLoadPath = text;
							this.RefreshClothingItemThumbnail(component);
						}
					}
				}
			}
			catch (Exception ex)
			{
				SuperController.LogError(string.Concat(new object[]
				{
					"Exception while reading clothing metafile ",
					text,
					" ",
					ex
				}), true);
			}
		}
	}

	// Token: 0x06004215 RID: 16917 RVA: 0x00130B4C File Offset: 0x0012ED4C
	public void RefreshDynamicClothes()
	{
		if (Application.isPlaying && this.dynamicClothingItemPrefab != null)
		{
			DAZClothingItem[] componentsInChildren = this.femaleClothingContainer.GetComponentsInChildren<DAZClothingItem>(true);
			this.SyncCustomClothingItems(DAZClothingItem.Gender.Female, componentsInChildren, "Custom/Clothing/Female/");
			DAZClothingItem[] componentsInChildren2 = this.maleClothingContainer.GetComponentsInChildren<DAZClothingItem>(true);
			this.SyncCustomClothingItems(DAZClothingItem.Gender.Male, componentsInChildren2, "Custom/Clothing/Male/");
			this.InitClothingItems();
			if (this.clothingSelectorFemaleUICustom != null)
			{
				this.clothingSelectorFemaleUICustom.GenerateStart();
				this.clothingSelectorFemaleUICustom.Generate();
				this.clothingSelectorFemaleUICustom.GenerateFinish();
			}
			if (this.clothingSelectorMaleUICustom != null)
			{
				this.clothingSelectorMaleUICustom.GenerateStart();
				this.clothingSelectorMaleUICustom.Generate();
				this.clothingSelectorMaleUICustom.GenerateFinish();
			}
		}
	}

	// Token: 0x06004216 RID: 16918 RVA: 0x00130C14 File Offset: 0x0012EE14
	public void InitClothingItems()
	{
		if (this.maleClothingContainer != null)
		{
			this._maleClothingItems = this.maleClothingContainer.GetComponentsInChildren<DAZClothingItem>(true);
		}
		else
		{
			this._maleClothingItems = new DAZClothingItem[0];
		}
		if (this.femaleClothingContainer != null)
		{
			this._femaleClothingItems = this.femaleClothingContainer.GetComponentsInChildren<DAZClothingItem>(true);
		}
		else
		{
			this._femaleClothingItems = new DAZClothingItem[0];
		}
		this._clothingItemById = new Dictionary<string, DAZClothingItem>();
		if (Application.isPlaying)
		{
			if (this.clothingItemJSONs == null)
			{
				this.clothingItemJSONs = new Dictionary<string, JSONStorableBool>();
			}
			if (this.clothingItemToggleJSONs == null)
			{
				this.clothingItemToggleJSONs = new List<JSONStorableAction>();
			}
		}
		DAZClothingItem[] clothingItems = this.clothingItems;
		for (int i = 0; i < clothingItems.Length; i++)
		{
			DAZClothingItem dc = clothingItems[i];
			DAZCharacterSelector $this = this;
			if (Application.isPlaying && !this.clothingItemJSONs.ContainsKey(dc.uid))
			{
				JSONStorableBool jsonstorableBool = new JSONStorableBool("clothing:" + dc.uid, dc.gameObject.activeSelf, new JSONStorableBool.SetJSONBoolCallback(this.SyncClothingItem));
				jsonstorableBool.altName = dc.uid;
				jsonstorableBool.isRestorable = false;
				jsonstorableBool.isStorable = false;
				base.RegisterBool(jsonstorableBool);
				this.clothingItemJSONs.Add(dc.uid, jsonstorableBool);
				JSONStorableAction jsonstorableAction = new JSONStorableAction("toggle:" + dc.uid, delegate()
				{
					$this.ToggleClothingItem(dc);
				});
				base.RegisterAction(jsonstorableAction);
				this.clothingItemToggleJSONs.Add(jsonstorableAction);
			}
			dc.characterSelector = this;
			if (this._clothingItemById.ContainsKey(dc.uid))
			{
				Debug.LogError("Duplicate uid found for clothing item " + dc.uid);
			}
			else
			{
				this._clothingItemById.Add(dc.uid, dc);
			}
			if (dc.gameObject.activeSelf)
			{
				dc.active = true;
			}
		}
		if (Application.isPlaying)
		{
			List<string> list = new List<string>();
			foreach (JSONStorableBool jsonstorableBool2 in this.clothingItemJSONs.Values)
			{
				string altName = jsonstorableBool2.altName;
				if (!this._clothingItemById.ContainsKey(altName))
				{
					base.DeregisterBool(jsonstorableBool2);
					list.Add(altName);
				}
			}
			foreach (string key in list)
			{
				this.clothingItemJSONs.Remove(key);
			}
			List<JSONStorableAction> list2 = new List<JSONStorableAction>();
			foreach (JSONStorableAction jsonstorableAction2 in this.clothingItemToggleJSONs)
			{
				string key2 = jsonstorableAction2.name.Replace("toggle:", string.Empty);
				if (!this._clothingItemById.ContainsKey(key2))
				{
					base.DeregisterAction(jsonstorableAction2);
				}
				else
				{
					list2.Add(jsonstorableAction2);
				}
			}
			this.clothingItemToggleJSONs = list2;
		}
	}

	// Token: 0x06004217 RID: 16919 RVA: 0x0002B482 File Offset: 0x00029682
	public void SyncClothingAdjustments()
	{
		this.SyncAnatomy();
	}

	// Token: 0x06004218 RID: 16920 RVA: 0x00130FC8 File Offset: 0x0012F1C8
	private void SyncAnatomy()
	{
		if (this._selectedCharacter != null)
		{
			bool flag = !this._disableAnatomy;
			foreach (DAZClothingItem dazclothingItem in this.clothingItems)
			{
				if (dazclothingItem != null && dazclothingItem.active && dazclothingItem.disableAnatomy)
				{
					flag = false;
					break;
				}
			}
			DAZSkinV2 skin = this._selectedCharacter.skin;
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				if (this.maleAnatomyComponents != null && this.maleAnatomyComponents.Length > 0)
				{
					foreach (DAZMaleAnatomy dazmaleAnatomy in this.maleAnatomyComponents)
					{
						Rigidbody[] componentsInChildren = dazmaleAnatomy.GetComponentsInChildren<Rigidbody>();
						foreach (Rigidbody rigidbody in componentsInChildren)
						{
							rigidbody.detectCollisions = flag;
						}
					}
				}
				if (skin != null)
				{
					foreach (int num in this.maleAnatomyOnMaterialSlots)
					{
						if (skin.materialsEnabled.Length > num)
						{
							skin.materialsEnabled[num] = flag;
						}
					}
					foreach (int num2 in this.maleAnatomyOffMaterialSlots)
					{
						if (skin.materialsEnabled.Length > num2)
						{
							skin.materialsEnabled[num2] = !flag;
						}
					}
				}
				if (skin != null && skin.dazMesh != null)
				{
					foreach (int num3 in this.maleAnatomyOnMaterialSlots)
					{
						if (skin.dazMesh.materialsEnabled.Length > num3)
						{
							skin.dazMesh.materialsEnabled[num3] = flag;
						}
					}
					foreach (int num5 in this.maleAnatomyOffMaterialSlots)
					{
						if (skin.dazMesh.materialsEnabled.Length > num5)
						{
							skin.dazMesh.materialsEnabled[num5] = !flag;
						}
					}
				}
			}
			else
			{
				if (skin != null)
				{
					foreach (int num7 in this.femaleAnatomyOnMaterialSlots)
					{
						if (skin.materialsEnabled.Length > num7)
						{
							skin.materialsEnabled[num7] = flag;
						}
					}
					foreach (int num9 in this.femaleAnatomyOffMaterialSlots)
					{
						if (skin.materialsEnabled.Length > num9)
						{
							skin.materialsEnabled[num9] = !flag;
						}
					}
				}
				if (skin != null && skin.dazMesh != null)
				{
					foreach (int num11 in this.femaleAnatomyOnMaterialSlots)
					{
						if (skin.dazMesh.materialsEnabled.Length > num11)
						{
							skin.dazMesh.materialsEnabled[num11] = flag;
						}
					}
					foreach (int num13 in this.femaleAnatomyOffMaterialSlots)
					{
						if (skin.dazMesh.materialsEnabled.Length > num13)
						{
							skin.dazMesh.materialsEnabled[num13] = !flag;
						}
					}
				}
				if (this.femaleBreastAdjustJoints != null)
				{
					float num14 = 1f;
					bool springDamperMultiplierOn = false;
					foreach (DAZClothingItem dazclothingItem2 in this.clothingItems)
					{
						if (dazclothingItem2 != null && dazclothingItem2.active && dazclothingItem2.adjustFemaleBreastJointSpringAndDamper && dazclothingItem2.jointAdjustEnabled)
						{
							springDamperMultiplierOn = true;
							if (dazclothingItem2.breastJointSpringAndDamperMultiplier > num14)
							{
								num14 = dazclothingItem2.breastJointSpringAndDamperMultiplier;
							}
						}
					}
					this.femaleBreastAdjustJoints.springDamperMultiplierOn = springDamperMultiplierOn;
					this.femaleBreastAdjustJoints.springDamperMultiplier = num14;
				}
				if (this.femaleGluteAdjustJoints != null)
				{
					float num16 = 1f;
					bool springDamperMultiplierOn2 = false;
					foreach (DAZClothingItem dazclothingItem3 in this.clothingItems)
					{
						if (dazclothingItem3 != null && dazclothingItem3.active && dazclothingItem3.adjustFemaleGluteJointSpringAndDamper && dazclothingItem3.jointAdjustEnabled)
						{
							springDamperMultiplierOn2 = true;
							if (dazclothingItem3.breastJointSpringAndDamperMultiplier > num16)
							{
								num16 = dazclothingItem3.gluteJointSpringAndDamperMultiplier;
							}
						}
					}
					this.femaleGluteAdjustJoints.springDamperMultiplierOn = springDamperMultiplierOn2;
					this.femaleGluteAdjustJoints.springDamperMultiplier = num16;
				}
			}
		}
	}

	// Token: 0x170008B5 RID: 2229
	// (get) Token: 0x06004219 RID: 16921 RVA: 0x0002B48A File Offset: 0x0002968A
	public DAZClothingItem[] maleClothingItems
	{
		get
		{
			this.Init(false);
			return this._maleClothingItems;
		}
	}

	// Token: 0x170008B6 RID: 2230
	// (get) Token: 0x0600421A RID: 16922 RVA: 0x0002B499 File Offset: 0x00029699
	public DAZClothingItem[] femaleClothingItems
	{
		get
		{
			this.Init(false);
			return this._femaleClothingItems;
		}
	}

	// Token: 0x170008B7 RID: 2231
	// (get) Token: 0x0600421B RID: 16923 RVA: 0x0002B4A8 File Offset: 0x000296A8
	public DAZClothingItem[] clothingItems
	{
		get
		{
			this.Init(false);
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this._maleClothingItems;
			}
			if (this.gender == DAZCharacterSelector.Gender.Female)
			{
				return this._femaleClothingItems;
			}
			return null;
		}
	}

	// Token: 0x0600421C RID: 16924 RVA: 0x001314C0 File Offset: 0x0012F6C0
	public DAZClothingItem GetClothingItem(string itemId)
	{
		if (this._clothingItemById == null)
		{
			this.Init(false);
		}
		DAZClothingItem result;
		if (this._clothingItemById.TryGetValue(itemId, out result))
		{
			return result;
		}
		return null;
	}

	// Token: 0x0600421D RID: 16925 RVA: 0x001314F8 File Offset: 0x0012F6F8
	public void EnableUndressAllClothingItems()
	{
		for (int i = 0; i < this.clothingItems.Length; i++)
		{
			DAZClothingItem dazclothingItem = this.clothingItems[i];
			ClothSimControl[] componentsInChildren = dazclothingItem.GetComponentsInChildren<ClothSimControl>();
			foreach (ClothSimControl clothSimControl in componentsInChildren)
			{
				clothSimControl.AllowDetach();
			}
		}
	}

	// Token: 0x0600421E RID: 16926 RVA: 0x00131558 File Offset: 0x0012F758
	public void RemoveAllClothing()
	{
		foreach (DAZClothingItem item in this.clothingItems)
		{
			this.SetActiveClothingItem(item, false, false);
		}
	}

	// Token: 0x0600421F RID: 16927 RVA: 0x0002B4D8 File Offset: 0x000296D8
	public void ToggleClothingItem(DAZClothingItem item)
	{
		this.SetActiveClothingItem(item, !item.active, false);
	}

	// Token: 0x06004220 RID: 16928 RVA: 0x00131590 File Offset: 0x0012F790
	public void SetActiveClothingItem(string itemId, bool active, bool fromRestore = false)
	{
		DAZClothingItem item;
		if (this._clothingItemById != null && this._clothingItemById.TryGetValue(itemId, out item))
		{
			this.SetActiveClothingItem(item, active, fromRestore);
		}
	}

	// Token: 0x06004221 RID: 16929 RVA: 0x001315C4 File Offset: 0x0012F7C4
	public void SetActiveClothingItem(DAZClothingItem item, bool active, bool fromRestore = false)
	{
		if (item != null)
		{
			if (active && fromRestore)
			{
				item.needsPostLoadJSONRestore = true;
			}
			item.active = active;
			DAZClothingItem.ExclusiveRegion exclusiveRegion = item.exclusiveRegion;
			if (active && exclusiveRegion != DAZClothingItem.ExclusiveRegion.None)
			{
				for (int i = 0; i < this.clothingItems.Length; i++)
				{
					if (this.clothingItems[i] != item && item.gender == this.clothingItems[i].gender && this.clothingItems[i].exclusiveRegion == exclusiveRegion && this.clothingItems[i].active)
					{
						this.SetActiveClothingItem(this.clothingItems[i], false, false);
					}
				}
			}
			item.gameObject.SetActive(active);
			if (this.clothingSelectorUIWrap != null)
			{
				this.clothingSelectorUIWrap.SetClothingItemToggle(item, active);
			}
			if (this.clothingSelectorUISim != null)
			{
				this.clothingSelectorUISim.SetClothingItemToggle(item, active);
			}
			if (this.clothingSelectorUICustom != null)
			{
				this.clothingSelectorUICustom.SetClothingItemToggle(item, active);
			}
			JSONStorableBool jsonstorableBool;
			if (this.clothingItemJSONs.TryGetValue(item.uid, out jsonstorableBool))
			{
				jsonstorableBool.val = active;
			}
			this.SyncAnatomy();
		}
	}

	// Token: 0x06004222 RID: 16930 RVA: 0x0002B4EB File Offset: 0x000296EB
	protected void SyncHairChoice(string choice)
	{
		this.SetSelectedHairGroup(choice, false);
	}

	// Token: 0x06004223 RID: 16931 RVA: 0x00131710 File Offset: 0x0012F910
	public void InitHair()
	{
		if (this.hairChooserJSON != null)
		{
			base.DeregisterStringChooser(this.hairChooserJSON);
		}
		if (this.maleHairContainer != null)
		{
			this._maleHairGroups = this.maleHairContainer.GetComponentsInChildren<DAZHairGroup>(true);
		}
		else
		{
			this._maleHairGroups = new DAZHairGroup[0];
		}
		if (this.femaleHairContainer != null)
		{
			this._femaleHairGroups = this.femaleHairContainer.GetComponentsInChildren<DAZHairGroup>(true);
		}
		else
		{
			this._femaleHairGroups = new DAZHairGroup[0];
		}
		List<string> list = new List<string>();
		string startingValue = string.Empty;
		foreach (DAZHairGroup dazhairGroup in this.hairGroups)
		{
			if (dazhairGroup.gameObject.activeSelf)
			{
				startingValue = dazhairGroup.displayName;
				this.selectedHairGroup = dazhairGroup;
			}
			list.Add(dazhairGroup.displayName);
		}
		if (Application.isPlaying)
		{
			this.hairChooserJSON = new JSONStorableStringChooser("hairSelection", list, startingValue, "Hair Selection", new JSONStorableStringChooser.SetStringCallback(this.SyncHairChoice));
			this.hairChooserJSON.isStorable = false;
			this.hairChooserJSON.isRestorable = false;
			base.RegisterStringChooser(this.hairChooserJSON);
		}
	}

	// Token: 0x170008B8 RID: 2232
	// (get) Token: 0x06004224 RID: 16932 RVA: 0x0002B4F5 File Offset: 0x000296F5
	public DAZHairGroup[] hairGroups
	{
		get
		{
			this.Init(false);
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this._maleHairGroups;
			}
			if (this.gender == DAZCharacterSelector.Gender.Female)
			{
				return this._femaleHairGroups;
			}
			return null;
		}
	}

	// Token: 0x06004225 RID: 16933 RVA: 0x00131848 File Offset: 0x0012FA48
	public void SetSelectedHairGroup(string hairGroupName, bool fromRestore = false)
	{
		foreach (DAZHairGroup dazhairGroup in this.hairGroups)
		{
			if (dazhairGroup.displayName == hairGroupName)
			{
				if (fromRestore)
				{
					dazhairGroup.needsPostLoadJSONRestore = true;
				}
				this.selectedHairGroup = dazhairGroup;
			}
		}
		if (this.hairChooserJSON != null)
		{
			this.hairChooserJSON.valNoCallback = hairGroupName;
		}
	}

	// Token: 0x170008B9 RID: 2233
	// (get) Token: 0x06004226 RID: 16934 RVA: 0x0002B525 File Offset: 0x00029725
	// (set) Token: 0x06004227 RID: 16935 RVA: 0x001318B0 File Offset: 0x0012FAB0
	public DAZHairGroup selectedHairGroup
	{
		get
		{
			this.Init(false);
			return this._selectedHairGroup;
		}
		set
		{
			if (this._selectedHairGroup != value)
			{
				if (this._selectedHairGroup != null)
				{
					this._selectedHairGroup.gameObject.SetActive(false);
				}
				this._selectedHairGroup = value;
				if (this._selectedHairGroup != null)
				{
					if (this.hairSelectorUI != null)
					{
						this.hairSelectorUI.SetActiveHairToggle(this._selectedHairGroup.displayName);
					}
					if (this.hairSelectorUIAlt != null)
					{
						this.hairSelectorUIAlt.SetActiveHairToggle(this._selectedHairGroup.displayName);
					}
					this._selectedHairGroup.gameObject.SetActive(true);
				}
			}
			else if (this._selectedHairGroup.needsPostLoadJSONRestore)
			{
				this._selectedHairGroup.gameObject.SetActive(false);
				this._selectedHairGroup.gameObject.SetActive(true);
			}
		}
	}

	// Token: 0x06004228 RID: 16936 RVA: 0x001319A0 File Offset: 0x0012FBA0
	public DAZHairGroup GetHairGroupByName(string hairGroupName)
	{
		foreach (DAZHairGroup dazhairGroup in this.hairGroups)
		{
			if (dazhairGroup.displayName == hairGroupName)
			{
				return dazhairGroup;
			}
		}
		return null;
	}

	// Token: 0x06004229 RID: 16937 RVA: 0x001319E0 File Offset: 0x0012FBE0
	public void OpenHairCustomizationUI(string hairName)
	{
		if (this.selectedHairGroup.displayName == hairName && this.selectedHairGroup.customizationUI != null && SuperController.singleton != null)
		{
			SuperController.singleton.SetCustomUI(this.selectedHairGroup.customizationUI);
		}
	}

	// Token: 0x170008BA RID: 2234
	// (get) Token: 0x0600422A RID: 16938 RVA: 0x0002B534 File Offset: 0x00029734
	public GenerateDAZMorphsControlUI morphsControlUI
	{
		get
		{
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this.morphsControlMaleUI;
			}
			return this.morphsControlFemaleUI;
		}
	}

	// Token: 0x170008BB RID: 2235
	// (get) Token: 0x0600422B RID: 16939 RVA: 0x0002B54F File Offset: 0x0002974F
	public GenerateDAZMorphsControlUI morphsControlUIAlt
	{
		get
		{
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this.morphsControlMaleUIAlt;
			}
			return this.morphsControlFemaleUIAlt;
		}
	}

	// Token: 0x170008BC RID: 2236
	// (get) Token: 0x0600422C RID: 16940 RVA: 0x0002B56A File Offset: 0x0002976A
	public GenerateDAZClothingSelectorUI clothingSelectorUIWrap
	{
		get
		{
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this.clothingSelectorMaleUIWrap;
			}
			return this.clothingSelectorFemaleUIWrap;
		}
	}

	// Token: 0x170008BD RID: 2237
	// (get) Token: 0x0600422D RID: 16941 RVA: 0x0002B585 File Offset: 0x00029785
	public GenerateDAZClothingSelectorUI clothingSelectorUISim
	{
		get
		{
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this.clothingSelectorMaleUISim;
			}
			return this.clothingSelectorFemaleUISim;
		}
	}

	// Token: 0x170008BE RID: 2238
	// (get) Token: 0x0600422E RID: 16942 RVA: 0x0002B5A0 File Offset: 0x000297A0
	public GenerateDAZClothingSelectorUI clothingSelectorUICustom
	{
		get
		{
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this.clothingSelectorMaleUICustom;
			}
			return this.clothingSelectorFemaleUICustom;
		}
	}

	// Token: 0x170008BF RID: 2239
	// (get) Token: 0x0600422F RID: 16943 RVA: 0x0002B5BB File Offset: 0x000297BB
	public GenerateDAZHairSelectorUI hairSelectorUI
	{
		get
		{
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this.hairSelectorMaleUI;
			}
			return this.hairSelectorFemaleUI;
		}
	}

	// Token: 0x170008C0 RID: 2240
	// (get) Token: 0x06004230 RID: 16944 RVA: 0x0002B5D6 File Offset: 0x000297D6
	public GenerateDAZHairSelectorUI hairSelectorUIAlt
	{
		get
		{
			if (this.gender == DAZCharacterSelector.Gender.Male)
			{
				return this.hairSelectorMaleUIAlt;
			}
			return this.hairSelectorFemaleUIAlt;
		}
	}

	// Token: 0x06004231 RID: 16945 RVA: 0x00131A40 File Offset: 0x0012FC40
	private IEnumerator ExportOBJHelper()
	{
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		this._characterRun.doSnap = false;
		OBJExporter oe = base.GetComponent<OBJExporter>();
		oe.Export(this.selectedCharacter.name + ".obj", this.exportSkin.GetMesh(), this._characterRun.snappedMorphedUVVertices, this._characterRun.snappedMorphedUVNormals, this.exportSkin.dazMesh.materials);
		oe.Export(this.selectedCharacter.name + "_skinned.obj", this.exportSkin.GetMesh(), this._characterRun.snappedSkinnedVertices, this._characterRun.snappedSkinnedNormals, this.exportSkin.dazMesh.materials);
		yield break;
	}

	// Token: 0x06004232 RID: 16946 RVA: 0x00131A5C File Offset: 0x0012FC5C
	public void ExportCurrentCharacterOBJ()
	{
		OBJExporter component = base.GetComponent<OBJExporter>();
		DAZMergedSkinV2 componentInChildren = this.selectedCharacter.GetComponentInChildren<DAZMergedSkinV2>();
		if (componentInChildren != null && component != null && this._characterRun != null)
		{
			this.exportSkin = componentInChildren;
			this._characterRun.doSnap = true;
			base.StartCoroutine(this.ExportOBJHelper());
		}
	}

	// Token: 0x06004233 RID: 16947 RVA: 0x00131AC8 File Offset: 0x0012FCC8
	public void CopyUI()
	{
		if (this.copyUIFrom != null)
		{
			this.color1DisplayNameText = this.copyUIFrom.color1DisplayNameText;
			this.color1Picker = this.copyUIFrom.color1Picker;
			this.color1Container = this.copyUIFrom.color1Container;
			this.color2DisplayNameText = this.copyUIFrom.color2DisplayNameText;
			this.color2Picker = this.copyUIFrom.color2Picker;
			this.color2Container = this.copyUIFrom.color2Container;
			this.color3DisplayNameText = this.copyUIFrom.color3DisplayNameText;
			this.color3Picker = this.copyUIFrom.color3Picker;
			this.color3Container = this.copyUIFrom.color3Container;
			this.param1DisplayNameText = this.copyUIFrom.param1DisplayNameText;
			this.param1Slider = this.copyUIFrom.param1Slider;
			this.param1DisplayNameTextAlt = this.copyUIFrom.param1DisplayNameTextAlt;
			this.param1SliderAlt = this.copyUIFrom.param1SliderAlt;
			this.param2DisplayNameText = this.copyUIFrom.param2DisplayNameText;
			this.param2Slider = this.copyUIFrom.param2Slider;
			this.param2DisplayNameTextAlt = this.copyUIFrom.param2DisplayNameTextAlt;
			this.param2SliderAlt = this.copyUIFrom.param2SliderAlt;
			this.param3DisplayNameText = this.copyUIFrom.param3DisplayNameText;
			this.param3Slider = this.copyUIFrom.param3Slider;
			this.param3DisplayNameTextAlt = this.copyUIFrom.param3DisplayNameTextAlt;
			this.param3SliderAlt = this.copyUIFrom.param3SliderAlt;
			this.param4DisplayNameText = this.copyUIFrom.param4DisplayNameText;
			this.param4Slider = this.copyUIFrom.param4Slider;
			this.param4DisplayNameTextAlt = this.copyUIFrom.param4DisplayNameTextAlt;
			this.param4SliderAlt = this.copyUIFrom.param4SliderAlt;
			this.param5DisplayNameText = this.copyUIFrom.param5DisplayNameText;
			this.param5Slider = this.copyUIFrom.param5Slider;
			this.param5DisplayNameTextAlt = this.copyUIFrom.param5DisplayNameTextAlt;
			this.param5SliderAlt = this.copyUIFrom.param5SliderAlt;
			this.param6DisplayNameText = this.copyUIFrom.param6DisplayNameText;
			this.param6Slider = this.copyUIFrom.param6Slider;
			this.param6DisplayNameTextAlt = this.copyUIFrom.param6DisplayNameTextAlt;
			this.param6SliderAlt = this.copyUIFrom.param6SliderAlt;
			this.param7DisplayNameText = this.copyUIFrom.param7DisplayNameText;
			this.param7Slider = this.copyUIFrom.param7Slider;
			this.param7DisplayNameTextAlt = this.copyUIFrom.param7DisplayNameTextAlt;
			this.param7SliderAlt = this.copyUIFrom.param7SliderAlt;
			this.param8DisplayNameText = this.copyUIFrom.param8DisplayNameText;
			this.param8Slider = this.copyUIFrom.param8Slider;
			this.param8DisplayNameTextAlt = this.copyUIFrom.param8DisplayNameTextAlt;
			this.param8SliderAlt = this.copyUIFrom.param8SliderAlt;
			this.param9DisplayNameText = this.copyUIFrom.param9DisplayNameText;
			this.param9Slider = this.copyUIFrom.param9Slider;
			this.param9DisplayNameTextAlt = this.copyUIFrom.param9DisplayNameTextAlt;
			this.param9SliderAlt = this.copyUIFrom.param9SliderAlt;
			this.param10DisplayNameText = this.copyUIFrom.param10DisplayNameText;
			this.param10Slider = this.copyUIFrom.param10Slider;
			this.param10DisplayNameTextAlt = this.copyUIFrom.param10DisplayNameTextAlt;
			this.param10SliderAlt = this.copyUIFrom.param10SliderAlt;
			this.textureGroup1Popup = this.copyUIFrom.textureGroup1Popup;
			this.textureGroup1PopupAlt = this.copyUIFrom.textureGroup1PopupAlt;
			this.textureGroup2Popup = this.copyUIFrom.textureGroup2Popup;
			this.textureGroup2PopupAlt = this.copyUIFrom.textureGroup2PopupAlt;
			this.textureGroup3Popup = this.copyUIFrom.textureGroup3Popup;
			this.textureGroup3PopupAlt = this.copyUIFrom.textureGroup3PopupAlt;
			this.textureGroup4Popup = this.copyUIFrom.textureGroup4Popup;
			this.textureGroup4PopupAlt = this.copyUIFrom.textureGroup4PopupAlt;
			this.textureGroup5Popup = this.copyUIFrom.textureGroup5Popup;
			this.textureGroup5PopupAlt = this.copyUIFrom.textureGroup5PopupAlt;
		}
	}

	// Token: 0x06004234 RID: 16948 RVA: 0x00131ED4 File Offset: 0x001300D4
	private void DisconnectCharacterOptionsUI()
	{
		if (this._selectedCharacter != null)
		{
			DAZCharacterMaterialOptions componentInChildren = this._selectedCharacter.GetComponentInChildren<DAZCharacterMaterialOptions>();
			if (componentInChildren != null)
			{
				componentInChildren.DeregisterUI();
			}
			DAZCharacterTextureControl componentInChildren2 = this._selectedCharacter.GetComponentInChildren<DAZCharacterTextureControl>();
			if (componentInChildren2 != null)
			{
				componentInChildren2.DeregisterUI();
				componentInChildren2.DeregisterUIAlt();
			}
		}
	}

	// Token: 0x06004235 RID: 16949 RVA: 0x00131F34 File Offset: 0x00130134
	private void ConnectCharacterMaterialOptionsUI()
	{
		if (Application.isPlaying && this._selectedCharacter != null)
		{
			DAZCharacterMaterialOptions componentInChildren = this._selectedCharacter.GetComponentInChildren<DAZCharacterMaterialOptions>();
			if (componentInChildren != null)
			{
				componentInChildren.CheckAwake();
				componentInChildren.color1DisplayNameText = this.color1DisplayNameText;
				componentInChildren.color1Picker = this.color1Picker;
				componentInChildren.color1Container = this.color1Container;
				componentInChildren.color2DisplayNameText = this.color2DisplayNameText;
				componentInChildren.color2Picker = this.color2Picker;
				componentInChildren.color2Container = this.color2Container;
				componentInChildren.color3DisplayNameText = this.color3DisplayNameText;
				componentInChildren.color3Picker = this.color3Picker;
				componentInChildren.color3Container = this.color3Container;
				componentInChildren.param1DisplayNameText = this.param1DisplayNameText;
				componentInChildren.param1Slider = this.param1Slider;
				componentInChildren.param1DisplayNameTextAlt = this.param1DisplayNameTextAlt;
				componentInChildren.param1SliderAlt = this.param1SliderAlt;
				componentInChildren.param2DisplayNameText = this.param2DisplayNameText;
				componentInChildren.param2Slider = this.param2Slider;
				componentInChildren.param2DisplayNameTextAlt = this.param2DisplayNameTextAlt;
				componentInChildren.param2SliderAlt = this.param2SliderAlt;
				componentInChildren.param3DisplayNameText = this.param3DisplayNameText;
				componentInChildren.param3Slider = this.param3Slider;
				componentInChildren.param3DisplayNameTextAlt = this.param3DisplayNameTextAlt;
				componentInChildren.param3SliderAlt = this.param3SliderAlt;
				componentInChildren.param4DisplayNameText = this.param4DisplayNameText;
				componentInChildren.param4Slider = this.param4Slider;
				componentInChildren.param4DisplayNameTextAlt = this.param4DisplayNameTextAlt;
				componentInChildren.param4SliderAlt = this.param4SliderAlt;
				componentInChildren.param5DisplayNameText = this.param5DisplayNameText;
				componentInChildren.param5Slider = this.param5Slider;
				componentInChildren.param5DisplayNameTextAlt = this.param5DisplayNameTextAlt;
				componentInChildren.param5SliderAlt = this.param5SliderAlt;
				componentInChildren.param6DisplayNameText = this.param6DisplayNameText;
				componentInChildren.param6Slider = this.param6Slider;
				componentInChildren.param6DisplayNameTextAlt = this.param6DisplayNameTextAlt;
				componentInChildren.param6SliderAlt = this.param6SliderAlt;
				componentInChildren.param7DisplayNameText = this.param7DisplayNameText;
				componentInChildren.param7Slider = this.param7Slider;
				componentInChildren.param7DisplayNameTextAlt = this.param7DisplayNameTextAlt;
				componentInChildren.param7SliderAlt = this.param7SliderAlt;
				componentInChildren.param8DisplayNameText = this.param8DisplayNameText;
				componentInChildren.param8Slider = this.param8Slider;
				componentInChildren.param8DisplayNameTextAlt = this.param8DisplayNameTextAlt;
				componentInChildren.param8SliderAlt = this.param8SliderAlt;
				componentInChildren.param9DisplayNameText = this.param9DisplayNameText;
				componentInChildren.param9Slider = this.param9Slider;
				componentInChildren.param9DisplayNameTextAlt = this.param9DisplayNameTextAlt;
				componentInChildren.param9SliderAlt = this.param9SliderAlt;
				componentInChildren.param10DisplayNameText = this.param10DisplayNameText;
				componentInChildren.param10Slider = this.param10Slider;
				componentInChildren.param10DisplayNameTextAlt = this.param10DisplayNameTextAlt;
				componentInChildren.param10SliderAlt = this.param10SliderAlt;
				componentInChildren.textureGroup1Popup = this.textureGroup1Popup;
				componentInChildren.textureGroup1PopupAlt = this.textureGroup1PopupAlt;
				componentInChildren.textureGroup2Popup = this.textureGroup2Popup;
				componentInChildren.textureGroup2PopupAlt = this.textureGroup2PopupAlt;
				componentInChildren.textureGroup3Popup = this.textureGroup3Popup;
				componentInChildren.textureGroup3PopupAlt = this.textureGroup3PopupAlt;
				componentInChildren.textureGroup4Popup = this.textureGroup4Popup;
				componentInChildren.textureGroup4PopupAlt = this.textureGroup4PopupAlt;
				componentInChildren.textureGroup5Popup = this.textureGroup5Popup;
				componentInChildren.textureGroup5PopupAlt = this.textureGroup5PopupAlt;
				componentInChildren.restoreAllFromDefaultsAction.button = this.restoreMaterialFromDefaultsButton;
				componentInChildren.restoreAllFromDefaultsAction.buttonAlt = this.restoreMaterialFromDefaultsButtonAlt;
				componentInChildren.restoreAllFromStore1Action.button = this.restoreMaterialFromStore1Button;
				componentInChildren.restoreAllFromStore1Action.buttonAlt = this.restoreMaterialFromStore1ButtonAlt;
				componentInChildren.restoreAllFromStore2Action.button = this.restoreMaterialFromStore2Button;
				componentInChildren.restoreAllFromStore2Action.buttonAlt = this.restoreMaterialFromStore2ButtonAlt;
				componentInChildren.restoreAllFromStore3Action.button = this.restoreMaterialFromStore3Button;
				componentInChildren.restoreAllFromStore3Action.buttonAlt = this.restoreMaterialFromStore3ButtonAlt;
				componentInChildren.saveToStore1Action.button = this.saveMaterialToStore1Button;
				componentInChildren.saveToStore1Action.buttonAlt = this.saveMaterialToStore1ButtonAlt;
				componentInChildren.saveToStore2Action.button = this.saveMaterialToStore2Button;
				componentInChildren.saveToStore2Action.buttonAlt = this.saveMaterialToStore2ButtonAlt;
				componentInChildren.saveToStore3Action.button = this.saveMaterialToStore3Button;
				componentInChildren.saveToStore3Action.buttonAlt = this.saveMaterialToStore3ButtonAlt;
				componentInChildren.InitUI();
				componentInChildren.InitUIAlt();
			}
			DAZCharacterTextureControl componentInChildren2 = this._selectedCharacter.GetComponentInChildren<DAZCharacterTextureControl>();
			if (componentInChildren2 != null)
			{
				if (this.characterTextureUITab != null)
				{
					this.characterTextureUITab.gameObject.SetActive(true);
				}
				if (this.characterTextureUI != null)
				{
					componentInChildren2.SetUI(this.characterTextureUI.transform);
				}
				if (this.characterTextureUITabAlt != null)
				{
					this.characterTextureUITabAlt.gameObject.SetActive(true);
				}
				if (this.characterTextureUIAlt != null)
				{
					componentInChildren2.SetUIAlt(this.characterTextureUIAlt.transform);
				}
			}
			else
			{
				if (this.characterTextureUITab != null)
				{
					this.characterTextureUITab.gameObject.SetActive(false);
				}
				if (this.characterTextureUITabAlt != null)
				{
					this.characterTextureUITabAlt.gameObject.SetActive(false);
				}
			}
		}
	}

	// Token: 0x06004236 RID: 16950 RVA: 0x00132424 File Offset: 0x00130624
	public void InitComponents()
	{
		this._eyelidControl = base.GetComponentInChildren<DAZMeshEyelidControl>();
		this._characterRun = base.GetComponentInChildren<DAZCharacterRun>();
		this._physicsMeshes = base.GetComponentsInChildren<DAZPhysicsMesh>(true);
		this._setDAZMorphs = base.GetComponentsInChildren<SetDAZMorph>(true);
		this._autoColliderBatchUpdaters = this.rootBones.GetComponentsInChildren<AutoColliderBatchUpdater>(true);
		this._autoColliderGroups = this.rootBones.GetComponentsInChildren<AutoColliderGroup>(true);
		this._autoColliders = this.rootBones.GetComponentsInChildren<AutoCollider>(true);
		this._setAnchorFromVertexComps = this.rootBones.GetComponentsInChildren<SetAnchorFromVertex>(true);
		this._ignoreChildColliders = this.rootBones.GetComponentsInChildren<IgnoreChildColliders>(true);
		List<DAZCharacterMaterialOptions> list = new List<DAZCharacterMaterialOptions>();
		DAZCharacterMaterialOptions[] componentsInChildren = base.GetComponentsInChildren<DAZCharacterMaterialOptions>();
		foreach (DAZCharacterMaterialOptions dazcharacterMaterialOptions in componentsInChildren)
		{
			DAZCharacter component = dazcharacterMaterialOptions.GetComponent<DAZCharacter>();
			DAZSkinV2 component2 = dazcharacterMaterialOptions.GetComponent<DAZSkinV2>();
			if (component == null && component2 == null && dazcharacterMaterialOptions != this.femaleEyelashMaterialOptions && dazcharacterMaterialOptions != this.maleEyelashMaterialOptions)
			{
				list.Add(dazcharacterMaterialOptions);
			}
		}
		this._materialOptions = list.ToArray();
	}

	// Token: 0x06004237 RID: 16951 RVA: 0x00132548 File Offset: 0x00130748
	public void SetMorphAnimatable(DAZMorph dm)
	{
		if (dm.animatable)
		{
			base.RegisterFloat(dm.jsonFloat);
		}
		else
		{
			base.DeregisterFloat(dm.jsonFloat);
			if (SuperController.singleton != null)
			{
				SuperController.singleton.ValidateAllAtoms();
			}
		}
	}

	// Token: 0x06004238 RID: 16952 RVA: 0x00132598 File Offset: 0x00130798
	protected void InitMorphBanks()
	{
		if (Application.isPlaying)
		{
			if (this.morphBankContainer == null)
			{
				this.morphBankContainer = base.transform;
			}
			if (this.femaleMorphBank1 == null && this.femaleMorphBank1Prefab != null)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.femaleMorphBank1Prefab.gameObject);
				gameObject.transform.SetParent(this.morphBankContainer);
				this.femaleMorphBank1 = gameObject.GetComponent<DAZMorphBank>();
				this.femaleMorphBank1.morphBones = this.rootBones;
				this.femaleMorphBank1.Init();
				if (this.morphsControlFemaleUI != null)
				{
					this.morphsControlFemaleUI.morphBank1 = this.femaleMorphBank1;
				}
				if (this.morphsControlFemaleUIAlt != null)
				{
					this.morphsControlFemaleUIAlt.morphBank1 = this.femaleMorphBank1;
				}
			}
			if (this.femaleMorphBank2 == null && this.femaleMorphBank2Prefab != null)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.femaleMorphBank2Prefab.gameObject);
				gameObject2.transform.SetParent(this.morphBankContainer);
				this.femaleMorphBank2 = gameObject2.GetComponent<DAZMorphBank>();
				this.femaleMorphBank2.morphBones = this.rootBones;
				this.femaleMorphBank2.Init();
				if (this.morphsControlFemaleUI != null)
				{
					this.morphsControlFemaleUI.morphBank2 = this.femaleMorphBank2;
				}
				if (this.morphsControlFemaleUIAlt != null)
				{
					this.morphsControlFemaleUIAlt.morphBank2 = this.femaleMorphBank2;
				}
			}
			if (this.femaleMorphBank3 == null && this.femaleMorphBank3Prefab != null)
			{
				GameObject gameObject3 = UnityEngine.Object.Instantiate<GameObject>(this.femaleMorphBank3Prefab.gameObject);
				gameObject3.transform.SetParent(this.morphBankContainer);
				this.femaleMorphBank3 = gameObject3.GetComponent<DAZMorphBank>();
				this.femaleMorphBank3.morphBones = this.rootBones;
				this.femaleMorphBank3.Init();
			}
			if (this.maleMorphBank1 == null && this.maleMorphBank1Prefab != null)
			{
				GameObject gameObject4 = UnityEngine.Object.Instantiate<GameObject>(this.maleMorphBank1Prefab.gameObject);
				gameObject4.transform.SetParent(this.morphBankContainer);
				this.maleMorphBank1 = gameObject4.GetComponent<DAZMorphBank>();
				this.maleMorphBank1.morphBones = this.rootBones;
				this.maleMorphBank1.Init();
				if (this.morphsControlMaleUI != null)
				{
					this.morphsControlMaleUI.morphBank1 = this.maleMorphBank1;
				}
				if (this.morphsControlMaleUIAlt != null)
				{
					this.morphsControlMaleUIAlt.morphBank1 = this.maleMorphBank1;
				}
			}
			if (this.maleMorphBank2 == null && this.maleMorphBank2Prefab != null)
			{
				GameObject gameObject5 = UnityEngine.Object.Instantiate<GameObject>(this.maleMorphBank2Prefab.gameObject);
				gameObject5.transform.SetParent(this.morphBankContainer);
				this.maleMorphBank2 = gameObject5.GetComponent<DAZMorphBank>();
				this.maleMorphBank2.morphBones = this.rootBones;
				this.maleMorphBank2.Init();
				if (this.morphsControlMaleUI != null)
				{
					this.morphsControlMaleUI.morphBank2 = this.maleMorphBank2;
				}
				if (this.morphsControlMaleUIAlt != null)
				{
					this.morphsControlMaleUIAlt.morphBank2 = this.maleMorphBank2;
				}
			}
			if (this.maleMorphBank3 == null && this.maleMorphBank3Prefab != null)
			{
				GameObject gameObject6 = UnityEngine.Object.Instantiate<GameObject>(this.maleMorphBank3Prefab.gameObject);
				gameObject6.transform.SetParent(this.morphBankContainer);
				this.maleMorphBank3 = gameObject6.GetComponent<DAZMorphBank>();
				this.maleMorphBank3.morphBones = this.rootBones;
				this.maleMorphBank3.Init();
			}
			List<string> morphDisplayNames = this.morphsControlFemaleUI.GetMorphDisplayNames();
			if (morphDisplayNames != null)
			{
				foreach (string morphDisplayName in morphDisplayNames)
				{
					DAZMorph morphByDisplayName = this.morphsControlFemaleUI.GetMorphByDisplayName(morphDisplayName);
					if (morphByDisplayName != null)
					{
						morphByDisplayName.setAnimatableCallback = new DAZMorph.SetAnimatableCallback(this.SetMorphAnimatable);
						if (morphByDisplayName.animatable)
						{
							base.RegisterFloat(morphByDisplayName.jsonFloat);
						}
					}
				}
			}
			List<string> morphDisplayNames2 = this.morphsControlMaleUI.GetMorphDisplayNames();
			if (morphDisplayNames2 != null)
			{
				foreach (string morphDisplayName2 in morphDisplayNames2)
				{
					DAZMorph morphByDisplayName2 = this.morphsControlMaleUI.GetMorphByDisplayName(morphDisplayName2);
					if (morphByDisplayName2 != null)
					{
						morphByDisplayName2.setAnimatableCallback = new DAZMorph.SetAnimatableCallback(this.SetMorphAnimatable);
						if (morphByDisplayName2.animatable)
						{
							base.RegisterFloat(morphByDisplayName2.jsonFloat);
						}
					}
				}
			}
		}
	}

	// Token: 0x06004239 RID: 16953 RVA: 0x00132AA0 File Offset: 0x00130CA0
	protected void InitJSONParams()
	{
		if (Application.isPlaying)
		{
			this.useAdvancedCollidersJSON = new JSONStorableBool("useAdvancedColliders", this._useAdvancedColliders, new JSONStorableBool.SetBoolCallback(this.SyncUseAdvancedColliders));
			this.useAdvancedCollidersJSON.storeType = JSONStorableParam.StoreType.Any;
			base.RegisterBool(this.useAdvancedCollidersJSON);
			this.useAuxBreastCollidersJSON = new JSONStorableBool("useAuxBreastColliders", this._useAuxBreastColliders, new JSONStorableBool.SetBoolCallback(this.SyncUseAuxBreastColliders));
			this.useAuxBreastCollidersJSON.storeType = JSONStorableParam.StoreType.Any;
			base.RegisterBool(this.useAuxBreastCollidersJSON);
			this.disableAnatomyJSON = new JSONStorableBool("disableAnatomy", this._disableAnatomy, new JSONStorableBool.SetBoolCallback(this.SyncDisableAnatomy));
			base.RegisterBool(this.disableAnatomyJSON);
		}
	}

	// Token: 0x0600423A RID: 16954 RVA: 0x00132B5C File Offset: 0x00130D5C
	public void Init(bool force = false)
	{
		if (!this.wasInit || force)
		{
			this.wasInit = true;
			this.InitMorphBanks();
			this.InitBones();
			this.InitComponents();
			this.RefreshDynamicClothes();
			this.InitClothingItems();
			this.InitHair();
			this.InitCharacters();
			this.SyncAnatomy();
		}
	}

	// Token: 0x0600423B RID: 16955 RVA: 0x00132BB4 File Offset: 0x00130DB4
	public override void InitUI()
	{
		if (this.UITransform != null)
		{
			DAZCharacterSelectorUI componentInChildren = this.UITransform.GetComponentInChildren<DAZCharacterSelectorUI>();
			if (componentInChildren != null)
			{
				this.useAdvancedCollidersJSON.toggle = componentInChildren.useAdvancedCollidersToggle;
				this.useAuxBreastCollidersJSON.toggle = componentInChildren.useAuxBreastCollidersToggle;
				this.disableAnatomyJSON.toggle = componentInChildren.disableAnatomyToggle;
			}
		}
	}

	// Token: 0x0600423C RID: 16956 RVA: 0x00132C20 File Offset: 0x00130E20
	protected IEnumerator PostEnable()
	{
		yield return null;
		this.ConnectSkin();
		yield break;
	}

	// Token: 0x0600423D RID: 16957 RVA: 0x00132C3C File Offset: 0x00130E3C
	private void OnDisable()
	{
		if (this._selectedCharacter != null)
		{
			DAZCharacter selectedCharacter = this._selectedCharacter;
			selectedCharacter.onLoadedHandlers = (JSONStorableDynamic.OnLoaded)Delegate.Remove(selectedCharacter.onLoadedHandlers, new JSONStorableDynamic.OnLoaded(this.OnCharacterLoaded));
		}
		if (this.onCharacterLoadedFlag != null)
		{
			this.onCharacterLoadedFlag.flag = true;
			this.onCharacterLoadedFlag = null;
		}
		if (this.delayResumeFlag != null)
		{
			this.delayResumeFlag.flag = true;
		}
	}

	// Token: 0x0600423E RID: 16958 RVA: 0x00132CB8 File Offset: 0x00130EB8
	private void OnEnable()
	{
		this.Init(false);
		if (this._selectedCharacter != null)
		{
			DAZCharacter selectedCharacter = this._selectedCharacter;
			selectedCharacter.onLoadedHandlers = (JSONStorableDynamic.OnLoaded)Delegate.Combine(selectedCharacter.onLoadedHandlers, new JSONStorableDynamic.OnLoaded(this.OnCharacterLoaded));
		}
		if (Application.isPlaying)
		{
			base.StartCoroutine(this.PostEnable());
		}
	}

	// Token: 0x0600423F RID: 16959 RVA: 0x0002B5F1 File Offset: 0x000297F1
	protected override void Awake()
	{
		if (!this.awakecalled)
		{
			base.Awake();
			this.Init(false);
			this.InitJSONParams();
			this.InitUI();
		}
	}

	// Token: 0x06004240 RID: 16960 RVA: 0x0002B617 File Offset: 0x00029817
	private void Start()
	{
		this.SyncGender();
		if (Application.isPlaying)
		{
			this.selectedCharacter = this.startingCharacter;
			this.selectedHairGroup = this.startingHairGroup;
			this.ResetClothing(false);
		}
	}

	// Token: 0x0400324A RID: 12874
	public DAZBones rootBones;

	// Token: 0x0400324B RID: 12875
	public Transform morphBankContainer;

	// Token: 0x0400324C RID: 12876
	public Transform femaleMorphBank1Prefab;

	// Token: 0x0400324D RID: 12877
	public Transform femaleMorphBank2Prefab;

	// Token: 0x0400324E RID: 12878
	public Transform femaleMorphBank3Prefab;

	// Token: 0x0400324F RID: 12879
	public Transform maleMorphBank1Prefab;

	// Token: 0x04003250 RID: 12880
	public Transform maleMorphBank2Prefab;

	// Token: 0x04003251 RID: 12881
	public Transform maleMorphBank3Prefab;

	// Token: 0x04003252 RID: 12882
	public DAZMorphBank femaleMorphBank1;

	// Token: 0x04003253 RID: 12883
	public DAZMorphBank femaleMorphBank2;

	// Token: 0x04003254 RID: 12884
	public DAZMorphBank femaleMorphBank3;

	// Token: 0x04003255 RID: 12885
	public DAZMorphBank maleMorphBank1;

	// Token: 0x04003256 RID: 12886
	public DAZMorphBank maleMorphBank2;

	// Token: 0x04003257 RID: 12887
	public DAZMorphBank maleMorphBank3;

	// Token: 0x04003258 RID: 12888
	[HideInInspector]
	[SerializeField]
	protected DAZCharacterSelector.Gender _gender;

	// Token: 0x04003259 RID: 12889
	public Transform customUIBucket;

	// Token: 0x0400325A RID: 12890
	public Transform maleCharactersContainer;

	// Token: 0x0400325B RID: 12891
	public Transform femaleCharactersContainer;

	// Token: 0x0400325C RID: 12892
	public Transform maleClothingContainer;

	// Token: 0x0400325D RID: 12893
	public Transform femaleClothingContainer;

	// Token: 0x0400325E RID: 12894
	public Transform maleHairContainer;

	// Token: 0x0400325F RID: 12895
	public Transform femaleHairContainer;

	// Token: 0x04003260 RID: 12896
	public Transform[] maleTransforms;

	// Token: 0x04003261 RID: 12897
	public Transform[] femaleTransforms;

	// Token: 0x04003262 RID: 12898
	public AdjustJoints femaleBreastAdjustJoints;

	// Token: 0x04003263 RID: 12899
	public AdjustJoints femaleGluteAdjustJoints;

	// Token: 0x04003264 RID: 12900
	public string rootBonesName = "Genesis2";

	// Token: 0x04003265 RID: 12901
	public string rootBonesNameMale = "Genesis2Male";

	// Token: 0x04003266 RID: 12902
	public string rootBonesNameFemale = "Genesis2Female";

	// Token: 0x04003267 RID: 12903
	public Collider[] auxBreastColliders;

	// Token: 0x04003268 RID: 12904
	protected JSONStorableBool useAuxBreastCollidersJSON;

	// Token: 0x04003269 RID: 12905
	[SerializeField]
	protected bool _useAuxBreastColliders = true;

	// Token: 0x0400326A RID: 12906
	public Transform[] regularColliders;

	// Token: 0x0400326B RID: 12907
	public Transform[] regularCollidersFemale;

	// Token: 0x0400326C RID: 12908
	public Transform[] regularCollidersMale;

	// Token: 0x0400326D RID: 12909
	public Transform[] advancedCollidersFemale;

	// Token: 0x0400326E RID: 12910
	public Transform[] advancedCollidersMale;

	// Token: 0x0400326F RID: 12911
	protected JSONStorableBool useAdvancedCollidersJSON;

	// Token: 0x04003270 RID: 12912
	[SerializeField]
	protected bool _useAdvancedColliders;

	// Token: 0x04003271 RID: 12913
	protected bool _disableAnatomy;

	// Token: 0x04003272 RID: 12914
	protected JSONStorableBool disableAnatomyJSON;

	// Token: 0x04003273 RID: 12915
	public int[] maleAnatomyOnMaterialSlots;

	// Token: 0x04003274 RID: 12916
	public int[] maleAnatomyOffMaterialSlots;

	// Token: 0x04003275 RID: 12917
	public int[] femaleAnatomyOnMaterialSlots;

	// Token: 0x04003276 RID: 12918
	public int[] femaleAnatomyOffMaterialSlots;

	// Token: 0x04003277 RID: 12919
	private DAZMaleAnatomy[] maleAnatomyComponents;

	// Token: 0x04003278 RID: 12920
	protected JSONStorableStringChooser characterChooserJSON;

	// Token: 0x04003279 RID: 12921
	public DAZCharacter startingCharacter;

	// Token: 0x0400327A RID: 12922
	private Dictionary<string, DAZCharacter> _characterByName;

	// Token: 0x0400327B RID: 12923
	private DAZCharacter[] _femaleCharacters;

	// Token: 0x0400327C RID: 12924
	private DAZCharacter[] _maleCharacters;

	// Token: 0x0400327D RID: 12925
	private DAZCharacter[] _characters;

	// Token: 0x0400327E RID: 12926
	private DAZCharacter _selectedCharacter;

	// Token: 0x0400327F RID: 12927
	protected AsyncFlag delayResumeFlag;

	// Token: 0x04003280 RID: 12928
	protected AsyncFlag onCharacterLoadedFlag;

	// Token: 0x04003281 RID: 12929
	protected Dictionary<string, JSONStorableBool> clothingItemJSONs;

	// Token: 0x04003282 RID: 12930
	protected List<JSONStorableAction> clothingItemToggleJSONs;

	// Token: 0x04003283 RID: 12931
	public Transform dynamicClothingItemPrefab;

	// Token: 0x04003284 RID: 12932
	private Dictionary<string, DAZClothingItem> _clothingItemById;

	// Token: 0x04003285 RID: 12933
	private DAZClothingItem[] _maleClothingItems;

	// Token: 0x04003286 RID: 12934
	private DAZClothingItem[] _femaleClothingItems;

	// Token: 0x04003287 RID: 12935
	protected JSONStorableStringChooser hairChooserJSON;

	// Token: 0x04003288 RID: 12936
	private DAZHairGroup[] _maleHairGroups;

	// Token: 0x04003289 RID: 12937
	private DAZHairGroup[] _femaleHairGroups;

	// Token: 0x0400328A RID: 12938
	public DAZHairGroup startingHairGroup;

	// Token: 0x0400328B RID: 12939
	private DAZHairGroup _selectedHairGroup;

	// Token: 0x0400328C RID: 12940
	public GenerateDAZCharacterSelectorUI characterSelectorUI;

	// Token: 0x0400328D RID: 12941
	public GenerateDAZCharacterSelectorUI characterSelectorUIAlt;

	// Token: 0x0400328E RID: 12942
	public GenerateDAZMorphsControlUI morphsControlFemaleUI;

	// Token: 0x0400328F RID: 12943
	public GenerateDAZMorphsControlUI morphsControlFemaleUIAlt;

	// Token: 0x04003290 RID: 12944
	public GenerateDAZMorphsControlUI morphsControlMaleUI;

	// Token: 0x04003291 RID: 12945
	public GenerateDAZMorphsControlUI morphsControlMaleUIAlt;

	// Token: 0x04003292 RID: 12946
	public GenerateDAZClothingSelectorUI clothingSelectorFemaleUIWrap;

	// Token: 0x04003293 RID: 12947
	public GenerateDAZClothingSelectorUI clothingSelectorFemaleUISim;

	// Token: 0x04003294 RID: 12948
	public GenerateDAZClothingSelectorUI clothingSelectorFemaleUICustom;

	// Token: 0x04003295 RID: 12949
	public GenerateDAZClothingSelectorUI clothingSelectorMaleUIWrap;

	// Token: 0x04003296 RID: 12950
	public GenerateDAZClothingSelectorUI clothingSelectorMaleUISim;

	// Token: 0x04003297 RID: 12951
	public GenerateDAZClothingSelectorUI clothingSelectorMaleUICustom;

	// Token: 0x04003298 RID: 12952
	public GenerateDAZHairSelectorUI hairSelectorFemaleUI;

	// Token: 0x04003299 RID: 12953
	public GenerateDAZHairSelectorUI hairSelectorFemaleUIAlt;

	// Token: 0x0400329A RID: 12954
	public GenerateDAZHairSelectorUI hairSelectorMaleUI;

	// Token: 0x0400329B RID: 12955
	public GenerateDAZHairSelectorUI hairSelectorMaleUIAlt;

	// Token: 0x0400329C RID: 12956
	public DAZCharacterTextureControlUI characterTextureUI;

	// Token: 0x0400329D RID: 12957
	public Transform characterTextureUITab;

	// Token: 0x0400329E RID: 12958
	public DAZCharacterTextureControlUI characterTextureUIAlt;

	// Token: 0x0400329F RID: 12959
	public Transform characterTextureUITabAlt;

	// Token: 0x040032A0 RID: 12960
	protected DAZSkinV2.SkinMethod saveSkinMethod;

	// Token: 0x040032A1 RID: 12961
	protected DAZSkinV2 exportSkin;

	// Token: 0x040032A2 RID: 12962
	public DAZCharacterMaterialOptions copyUIFrom;

	// Token: 0x040032A3 RID: 12963
	public Text color1DisplayNameText;

	// Token: 0x040032A4 RID: 12964
	public HSVColorPicker color1Picker;

	// Token: 0x040032A5 RID: 12965
	public RectTransform color1Container;

	// Token: 0x040032A6 RID: 12966
	public Text color2DisplayNameText;

	// Token: 0x040032A7 RID: 12967
	public HSVColorPicker color2Picker;

	// Token: 0x040032A8 RID: 12968
	public RectTransform color2Container;

	// Token: 0x040032A9 RID: 12969
	public Text color3DisplayNameText;

	// Token: 0x040032AA RID: 12970
	public HSVColorPicker color3Picker;

	// Token: 0x040032AB RID: 12971
	public RectTransform color3Container;

	// Token: 0x040032AC RID: 12972
	public Text param1DisplayNameText;

	// Token: 0x040032AD RID: 12973
	public Slider param1Slider;

	// Token: 0x040032AE RID: 12974
	public Text param1DisplayNameTextAlt;

	// Token: 0x040032AF RID: 12975
	public Slider param1SliderAlt;

	// Token: 0x040032B0 RID: 12976
	public Text param2DisplayNameText;

	// Token: 0x040032B1 RID: 12977
	public Slider param2Slider;

	// Token: 0x040032B2 RID: 12978
	public Text param2DisplayNameTextAlt;

	// Token: 0x040032B3 RID: 12979
	public Slider param2SliderAlt;

	// Token: 0x040032B4 RID: 12980
	public Text param3DisplayNameText;

	// Token: 0x040032B5 RID: 12981
	public Slider param3Slider;

	// Token: 0x040032B6 RID: 12982
	public Text param3DisplayNameTextAlt;

	// Token: 0x040032B7 RID: 12983
	public Slider param3SliderAlt;

	// Token: 0x040032B8 RID: 12984
	public Text param4DisplayNameText;

	// Token: 0x040032B9 RID: 12985
	public Slider param4Slider;

	// Token: 0x040032BA RID: 12986
	public Text param4DisplayNameTextAlt;

	// Token: 0x040032BB RID: 12987
	public Slider param4SliderAlt;

	// Token: 0x040032BC RID: 12988
	public Text param5DisplayNameText;

	// Token: 0x040032BD RID: 12989
	public Slider param5Slider;

	// Token: 0x040032BE RID: 12990
	public Text param5DisplayNameTextAlt;

	// Token: 0x040032BF RID: 12991
	public Slider param5SliderAlt;

	// Token: 0x040032C0 RID: 12992
	public Text param6DisplayNameText;

	// Token: 0x040032C1 RID: 12993
	public Slider param6Slider;

	// Token: 0x040032C2 RID: 12994
	public Text param6DisplayNameTextAlt;

	// Token: 0x040032C3 RID: 12995
	public Slider param6SliderAlt;

	// Token: 0x040032C4 RID: 12996
	public Text param7DisplayNameText;

	// Token: 0x040032C5 RID: 12997
	public Slider param7Slider;

	// Token: 0x040032C6 RID: 12998
	public Text param7DisplayNameTextAlt;

	// Token: 0x040032C7 RID: 12999
	public Slider param7SliderAlt;

	// Token: 0x040032C8 RID: 13000
	public Text param8DisplayNameText;

	// Token: 0x040032C9 RID: 13001
	public Slider param8Slider;

	// Token: 0x040032CA RID: 13002
	public Text param8DisplayNameTextAlt;

	// Token: 0x040032CB RID: 13003
	public Slider param8SliderAlt;

	// Token: 0x040032CC RID: 13004
	public Text param9DisplayNameText;

	// Token: 0x040032CD RID: 13005
	public Slider param9Slider;

	// Token: 0x040032CE RID: 13006
	public Text param9DisplayNameTextAlt;

	// Token: 0x040032CF RID: 13007
	public Slider param9SliderAlt;

	// Token: 0x040032D0 RID: 13008
	public Text param10DisplayNameText;

	// Token: 0x040032D1 RID: 13009
	public Slider param10Slider;

	// Token: 0x040032D2 RID: 13010
	public Text param10DisplayNameTextAlt;

	// Token: 0x040032D3 RID: 13011
	public Slider param10SliderAlt;

	// Token: 0x040032D4 RID: 13012
	public UIPopup textureGroup1Popup;

	// Token: 0x040032D5 RID: 13013
	public Text textureGroup1Text;

	// Token: 0x040032D6 RID: 13014
	public UIPopup textureGroup1PopupAlt;

	// Token: 0x040032D7 RID: 13015
	public Text textureGroup1TextAlt;

	// Token: 0x040032D8 RID: 13016
	public UIPopup textureGroup2Popup;

	// Token: 0x040032D9 RID: 13017
	public Text textureGroup2Text;

	// Token: 0x040032DA RID: 13018
	public UIPopup textureGroup2PopupAlt;

	// Token: 0x040032DB RID: 13019
	public Text textureGroup2TextAlt;

	// Token: 0x040032DC RID: 13020
	public UIPopup textureGroup3Popup;

	// Token: 0x040032DD RID: 13021
	public Text textureGroup3Text;

	// Token: 0x040032DE RID: 13022
	public UIPopup textureGroup3PopupAlt;

	// Token: 0x040032DF RID: 13023
	public Text textureGroup3TextAlt;

	// Token: 0x040032E0 RID: 13024
	public UIPopup textureGroup4Popup;

	// Token: 0x040032E1 RID: 13025
	public Text textureGroup4Text;

	// Token: 0x040032E2 RID: 13026
	public UIPopup textureGroup4PopupAlt;

	// Token: 0x040032E3 RID: 13027
	public Text textureGroup4TextAlt;

	// Token: 0x040032E4 RID: 13028
	public UIPopup textureGroup5Popup;

	// Token: 0x040032E5 RID: 13029
	public Text textureGroup5Text;

	// Token: 0x040032E6 RID: 13030
	public UIPopup textureGroup5PopupAlt;

	// Token: 0x040032E7 RID: 13031
	public Text textureGroup5TextAlt;

	// Token: 0x040032E8 RID: 13032
	public Button restoreMaterialFromDefaultsButton;

	// Token: 0x040032E9 RID: 13033
	public Button saveMaterialToStore1Button;

	// Token: 0x040032EA RID: 13034
	public Button restoreMaterialFromStore1Button;

	// Token: 0x040032EB RID: 13035
	public Button saveMaterialToStore2Button;

	// Token: 0x040032EC RID: 13036
	public Button restoreMaterialFromStore2Button;

	// Token: 0x040032ED RID: 13037
	public Button saveMaterialToStore3Button;

	// Token: 0x040032EE RID: 13038
	public Button restoreMaterialFromStore3Button;

	// Token: 0x040032EF RID: 13039
	public Button restoreMaterialFromDefaultsButtonAlt;

	// Token: 0x040032F0 RID: 13040
	public Button saveMaterialToStore1ButtonAlt;

	// Token: 0x040032F1 RID: 13041
	public Button restoreMaterialFromStore1ButtonAlt;

	// Token: 0x040032F2 RID: 13042
	public Button saveMaterialToStore2ButtonAlt;

	// Token: 0x040032F3 RID: 13043
	public Button restoreMaterialFromStore2ButtonAlt;

	// Token: 0x040032F4 RID: 13044
	public Button saveMaterialToStore3ButtonAlt;

	// Token: 0x040032F5 RID: 13045
	public Button restoreMaterialFromStore3ButtonAlt;

	// Token: 0x040032F6 RID: 13046
	private DAZMeshEyelidControl _eyelidControl;

	// Token: 0x040032F7 RID: 13047
	private DAZCharacterRun _characterRun;

	// Token: 0x040032F8 RID: 13048
	private AutoColliderBatchUpdater[] _autoColliderBatchUpdaters;

	// Token: 0x040032F9 RID: 13049
	private AutoColliderGroup[] _autoColliderGroups;

	// Token: 0x040032FA RID: 13050
	private AutoCollider[] _autoColliders;

	// Token: 0x040032FB RID: 13051
	private SetDAZMorph[] _setDAZMorphs;

	// Token: 0x040032FC RID: 13052
	private DAZPhysicsMesh[] _physicsMeshes;

	// Token: 0x040032FD RID: 13053
	private SetAnchorFromVertex[] _setAnchorFromVertexComps;

	// Token: 0x040032FE RID: 13054
	private DAZCharacterMaterialOptions[] _materialOptions;

	// Token: 0x040032FF RID: 13055
	public DAZCharacterMaterialOptions femaleEyelashMaterialOptions;

	// Token: 0x04003300 RID: 13056
	public DAZCharacterMaterialOptions maleEyelashMaterialOptions;

	// Token: 0x04003301 RID: 13057
	private IgnoreChildColliders[] _ignoreChildColliders;

	// Token: 0x04003302 RID: 13058
	private bool wasInit;

	// Token: 0x02000A81 RID: 2689
	public enum Gender
	{
		// Token: 0x04003304 RID: 13060
		None,
		// Token: 0x04003305 RID: 13061
		Male,
		// Token: 0x04003306 RID: 13062
		Female,
		// Token: 0x04003307 RID: 13063
		Both
	}
}
