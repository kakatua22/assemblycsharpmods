using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using MeshVR;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[Serializable]
public class DAZMorph
{
	public DAZMorph()
	{
	}

	public DAZMorph(DAZMorph copyFrom)
	{
		this.CopyParameters(copyFrom);
		this.numDeltas = copyFrom.numDeltas;
		this.deltas = new DAZMorphVertex[copyFrom.deltas.Length];
		for (int i = 0; i < this.deltas.Length; i++)
		{
			this.deltas[i] = new DAZMorphVertex();
			this.deltas[i].vertex = copyFrom.deltas[i].vertex;
			this.deltas[i].delta = copyFrom.deltas[i].delta;
		}
		this.formulas = new DAZMorphFormula[copyFrom.formulas.Length];
		for (int j = 0; j < this.formulas.Length; j++)
		{
			this.formulas[j] = new DAZMorphFormula();
			this.formulas[j].targetType = copyFrom.formulas[j].targetType;
			this.formulas[j].target = copyFrom.formulas[j].target;
			this.formulas[j].multiplier = copyFrom.formulas[j].multiplier;
		}
	}

	public string resolvedDisplayName
	{
		get
		{
			if (this.overrideName != null && this.overrideName != string.Empty)
			{
				return this.overrideName;
			}
			return this.morphName;
		}
	}

	public float morphValue
	{
		get
		{
			return this._morphValue;
		}
		set
		{
			if (this.jsonFloat != null)
			{
				this.jsonFloat.val = value;
				return;
			}
			this._morphValue = value;
		}
	}

	public float morphValueAdjustLimits
	{
		get
		{
			return this._morphValue;
		}
		set
		{
			if (value < this.min)
			{
				this.min = value;
				if (this.jsonFloat != null)
				{
					this.jsonFloat.min = value;
				}
			}
			else if (value > this.max)
			{
				this.max = value;
				if (this.jsonFloat != null)
				{
					this.jsonFloat.max = value;
				}
			}
			if (this.jsonFloat != null)
			{
				this.jsonFloat.val = value;
				return;
			}
			this._morphValue = value;
		}
	}

	public void SetAnimatable(bool b)
	{
		this.animatable = b;
	}

	public Toggle animatableToggle
	{
		get
		{
			return this._animatableToggle;
		}
		set
		{
			if (this._animatableToggle != value)
			{
				if (this._animatableToggle != null)
				{
					this._animatableToggle.onValueChanged.RemoveListener(new UnityAction<bool>(this.SetAnimatable));
				}
				this._animatableToggle = value;
				if (this._animatableToggle != null)
				{
					this._animatableToggle.isOn = this._animatable;
					this._animatableToggle.onValueChanged.AddListener(new UnityAction<bool>(this.SetAnimatable));
				}
			}
		}
	}

	public Toggle animatableToggleAlt
	{
		get
		{
			return this._animatableToggleAlt;
		}
		set
		{
			if (this._animatableToggleAlt != value)
			{
				if (this._animatableToggleAlt != null)
				{
					this._animatableToggleAlt.onValueChanged.RemoveListener(new UnityAction<bool>(this.SetAnimatable));
				}
				this._animatableToggleAlt = value;
				if (this._animatableToggleAlt != null)
				{
					this._animatableToggleAlt.isOn = this._animatable;
					this._animatableToggleAlt.onValueChanged.AddListener(new UnityAction<bool>(this.SetAnimatable));
				}
			}
		}
	}

	public bool animatable
	{
		get
		{
			return this._animatable;
		}
		set
		{
			if (this._animatable != value)
			{
				this._animatable = value;
				if (this._animatableToggle != null)
				{
					this._animatableToggle.isOn = this._animatable;
				}
				if (this._animatableToggleAlt != null)
				{
					this._animatableToggleAlt.isOn = this._animatable;
				}
				if (this.setAnimatableCallback != null)
				{
					this.setAnimatableCallback(this);
				}
			}
		}
	}

	public void SyncJSON()
	{
		this.jsonFloat.val = this._morphValue;
	}

	public void SetValue(float v)
	{
		this._morphValue = v;
	}

	public void Init()
	{
		this.startValue = this.morphValue;
		this._startingAnimatable = this._animatable;
		this.jsonFloat = new JSONStorableFloat(this.resolvedDisplayName, this._morphValue, new JSONStorableFloat.SetFloatCallback(this.SetValue), this.min, this.max, true, true);
		this.jsonFloat.isStorable = false;
		this.jsonFloat.isRestorable = false;
	}

	public Button increaseRangeButton
	{
		get
		{
			return this._increaseRangeButton;
		}
		set
		{
			if (this._increaseRangeButton != value)
			{
				if (this._increaseRangeButton != null)
				{
					this._increaseRangeButton.onClick.RemoveListener(new UnityAction(this.IncreaseRange));
				}
				this._increaseRangeButton = value;
				if (this._increaseRangeButton != null)
				{
					this._increaseRangeButton.onClick.AddListener(new UnityAction(this.IncreaseRange));
				}
			}
		}
	}

	public Button increaseRangeButtonAlt
	{
		get
		{
			return this._increaseRangeButtonAlt;
		}
		set
		{
			if (this._increaseRangeButtonAlt != value)
			{
				if (this._increaseRangeButtonAlt != null)
				{
					this._increaseRangeButtonAlt.onClick.RemoveListener(new UnityAction(this.IncreaseRange));
				}
				this._increaseRangeButtonAlt = value;
				if (this._increaseRangeButtonAlt != null)
				{
					this._increaseRangeButtonAlt.onClick.AddListener(new UnityAction(this.IncreaseRange));
				}
			}
		}
	}

	public void IncreaseRange()
	{
		this.jsonFloat.min += -1f;
		this.jsonFloat.max += 1f;
	}

	public Button resetRangeButton
	{
		get
		{
			return this._resetRangeButton;
		}
		set
		{
			if (this._resetRangeButton != value)
			{
				if (this._resetRangeButton != null)
				{
					this._resetRangeButton.onClick.RemoveListener(new UnityAction(this.ResetRange));
				}
				this._resetRangeButton = value;
				if (this._resetRangeButton != null)
				{
					this._resetRangeButton.onClick.AddListener(new UnityAction(this.ResetRange));
				}
			}
		}
	}

	public Button resetRangeButtonAlt
	{
		get
		{
			return this._resetRangeButtonAlt;
		}
		set
		{
			if (this._resetRangeButtonAlt != value)
			{
				if (this._resetRangeButtonAlt != null)
				{
					this._resetRangeButtonAlt.onClick.RemoveListener(new UnityAction(this.ResetRange));
				}
				this._resetRangeButtonAlt = value;
				if (this._resetRangeButtonAlt != null)
				{
					this._resetRangeButtonAlt.onClick.AddListener(new UnityAction(this.ResetRange));
				}
			}
		}
	}

	public void ResetRange()
	{
		this.jsonFloat.min = this.min;
		this.jsonFloat.max = this.max;
	}

	public void InitUI(Transform UITransform)
	{
		DAZMorphUI component = UITransform.GetComponent<DAZMorphUI>();
		if (component != null)
		{
			this.jsonFloat.slider = component.slider;
			this.increaseRangeButton = component.increaseRangeButton;
			this.resetRangeButton = component.resetRangeButton;
			if (component.morphNameText != null)
			{
				component.morphNameText.text = this.displayName;
			}
			this.animatableToggle = component.animatableToggle;
			if (this.animatableToggle != null)
			{
				bool flag = false;
				bool flag2 = false;
				DAZMorphFormula[] array = this.formulas;
				for (int i = 0; i < array.Length; i++)
				{
					DAZMorphFormulaTargetType targetType = array[i].targetType;
					if (targetType - DAZMorphFormulaTargetType.BoneCenterX > 2)
					{
						if (targetType - DAZMorphFormulaTargetType.OrientationX <= 2)
						{
							flag2 = true;
						}
					}
					else
					{
						flag = true;
					}
				}
				if (flag || flag2)
				{
					Text[] componentsInChildren = this.animatableToggle.GetComponentsInChildren<Text>();
					for (int i = 0; i < componentsInChildren.Length; i++)
					{
						componentsInChildren[i].color = Color.red;
					}
				}
			}
		}
	}

	public void DeregisterUI()
	{
		this.jsonFloat.slider = null;
		this.increaseRangeButton = null;
		this.resetRangeButton = null;
		this.animatableToggle = null;
	}

	public void InitUIAlt(Transform UITransform)
	{
		DAZMorphUI component = UITransform.GetComponent<DAZMorphUI>();
		if (component != null)
		{
			this.jsonFloat.sliderAlt = component.slider;
			this.increaseRangeButtonAlt = component.increaseRangeButton;
			this.resetRangeButtonAlt = component.resetRangeButton;
			if (component.morphNameText != null)
			{
				component.morphNameText.text = this.displayName;
			}
			this.animatableToggleAlt = component.animatableToggle;
			if (this.animatableToggleAlt != null)
			{
				bool flag = false;
				bool flag2 = false;
				DAZMorphFormula[] array = this.formulas;
				for (int i = 0; i < array.Length; i++)
				{
					DAZMorphFormulaTargetType targetType = array[i].targetType;
					if (targetType - DAZMorphFormulaTargetType.BoneCenterX > 2)
					{
						if (targetType - DAZMorphFormulaTargetType.OrientationX <= 2)
						{
							flag2 = true;
						}
					}
					else
					{
						flag = true;
					}
				}
				if (flag || flag2)
				{
					Text[] componentsInChildren = this.animatableToggleAlt.GetComponentsInChildren<Text>();
					for (int j = 0; j < componentsInChildren.Length; j++)
					{
						componentsInChildren[j].color = Color.red;
					}
				}
			}
		}
	}

	public void DeregisterUIAlt()
	{
		this.jsonFloat.sliderAlt = null;
		this.increaseRangeButtonAlt = null;
		this.resetRangeButtonAlt = null;
		this.animatableToggleAlt = null;
	}

	public bool StoreJSON(JSONClass jc, bool forceStore = false)
	{
		bool result = false;
		jc["name"] = this.resolvedDisplayName;
		if (this.jsonFloat.val != this.jsonFloat.defaultVal || forceStore)
		{
			jc["value"].AsFloat = this._morphValue;
			result = true;
		}
		if (this.jsonFloat.min != this.min || forceStore)
		{
			jc["min"].AsFloat = this.jsonFloat.min;
			result = true;
		}
		if (this.jsonFloat.max != this.max || forceStore)
		{
			jc["max"].AsFloat = this.jsonFloat.max;
			result = true;
		}
		if (this._animatable != this._startingAnimatable || forceStore)
		{
			jc["animatable"].AsBool = this._animatable;
			result = true;
		}
		return result;
	}

	// Token: 0x060040AD RID: 16557
	public void RestoreFromJSON(JSONClass jc)
	{
		if (jc["min"] != null)
		{
			this.jsonFloat.min = jc["min"].AsFloat;
		}
		if (jc["max"] != null)
		{
			this.jsonFloat.max = jc["max"].AsFloat;
		}
		if (jc["value"] != null)
		{
			this.jsonFloat.val = jc["value"].AsFloat;
		}
		if (jc["animatable"] != null)
		{
			this.animatable = jc["animatable"].AsBool;
		}
	}

	public void Reset()
	{
		if (this.jsonFloat != null)
		{
			this.jsonFloat.val = this.jsonFloat.defaultVal;
			this.jsonFloat.min = this.min;
			this.jsonFloat.max = this.max;
		}
	}

	public void SetDefaultValue()
	{
		if (this.jsonFloat != null)
		{
			this.jsonFloat.val = this.jsonFloat.defaultVal;
		}
	}

	public void CopyParameters(DAZMorph copyFrom)
	{
		this.group = copyFrom.group;
		this.region = copyFrom.region;
		this.morphName = copyFrom.morphName;
		this.displayName = copyFrom.displayName;
		this.overrideName = copyFrom.morphName;
		this.preserveValueOnReimport = copyFrom.preserveValueOnReimport;
		this.min = copyFrom.min;
		this.max = copyFrom.max;
		this.visible = copyFrom.visible;
		this.disable = copyFrom.disable;
		this.isPoseControl = copyFrom.isPoseControl;
		this._morphValue = copyFrom.morphValue;
		this.appliedValue = copyFrom.appliedValue;
		this.triggerNormalRecalc = copyFrom.triggerNormalRecalc;
		this.triggerTangentRecalc = copyFrom.triggerTangentRecalc;
	}

	private bool ProcessFormula(JSONNode fn, DAZMorphFormula formula, string morphName)
	{
		JSONNode jsonnode = fn["operations"];
		string text = DAZImport.DAZurlToId(jsonnode[0]["url"]);
		if (text == morphName + "?value")
		{
			string text2 = jsonnode[2]["op"];
			if (text2 == "mult")
			{
				float asFloat = jsonnode[1]["val"].AsFloat;
				formula.multiplier = asFloat;
				return true;
			}
			Debug.LogWarning("Morph " + morphName + ": Found unknown formula " + text2);
		}
		else if (formula.target == morphName)
		{
			string text3 = fn["stage"];
			if (text3 != null)
			{
				if (text3 == "mult")
				{
					formula.targetType = DAZMorphFormulaTargetType.MCMMult;
					text = Regex.Replace(text, "\\?.*", string.Empty);
					formula.target = text;
					return true;
				}
				Debug.LogWarning("Morph " + morphName + ": Found unknown stage " + text3);
			}
			else
			{
				formula.targetType = DAZMorphFormulaTargetType.MCM;
				text = Regex.Replace(text, "\\?.*", string.Empty);
				formula.target = text;
				string text4 = jsonnode[2]["op"];
				if (text4 == "mult")
				{
					float asFloat2 = jsonnode[1]["val"].AsFloat;
					formula.multiplier = asFloat2;
					return true;
				}
				Debug.LogWarning("Morph " + morphName + ": Found unknown formula " + text4);
			}
		}
		else
		{
			Debug.LogWarning("Morph " + morphName + ": Found unknown operation url " + text);
		}
		return false;
	}

	public void Import(JSONNode mn)
	{
		this.morphName = mn["id"];
		this._morphValue = 0f;
		this.appliedValue = 0f;
		if (mn["group"] != null)
		{
			this.group = Regex.Replace(mn["group"], "^/", string.Empty);
		}
		else
		{
			this.group = string.Empty;
		}
		this.displayName = mn["channel"]["label"];
		this.region = mn["region"];
		if (this.region == null)
		{
			this.region = this.group;
		}
		this.min = mn["channel"]["min"].AsFloat;
		if (this.min == -100000f)
		{
			this.min = -1f;
		}
		this.max = mn["channel"]["max"].AsFloat;
		if (this.max == 100000f)
		{
			this.max = 1f;
		}
		if (mn["formulas"].Count > 0)
		{
			List<DAZMorphFormula> list = new List<DAZMorphFormula>();
			IEnumerator enumerator = mn["formulas"].AsArray.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					JSONNode jsonnode = (JSONNode)obj;
					DAZMorphFormula dazmorphFormula = new DAZMorphFormula();
					string text = jsonnode["output"];
					string text2 = Regex.Replace(text, "^.*#", string.Empty);
					text2 = Regex.Replace(text2, "\\?.*", string.Empty);
					text2 = DAZImport.DAZurlFix(text2);
					dazmorphFormula.target = text2;
					string text3 = Regex.Replace(text, "^.*\\?", string.Empty);
					if (text3 == "value")
					{
						dazmorphFormula.targetType = DAZMorphFormulaTargetType.MorphValue;
						if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
						{
							list.Add(dazmorphFormula);
						}
					}
					else if (text3 == "scale/general")
					{
						dazmorphFormula.targetType = DAZMorphFormulaTargetType.GeneralScale;
						if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
						{
							list.Add(dazmorphFormula);
						}
					}
					else if (text3 == "scale/x")
					{
						dazmorphFormula.targetType = DAZMorphFormulaTargetType.ScaleX;
						if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
						{
							list.Add(dazmorphFormula);
						}
					}
					else if (text3 == "scale/y")
					{
						dazmorphFormula.targetType = DAZMorphFormulaTargetType.ScaleY;
						if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
						{
							list.Add(dazmorphFormula);
						}
					}
					else if (text3 == "scale/z")
					{
						dazmorphFormula.targetType = DAZMorphFormulaTargetType.ScaleZ;
						if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
						{
							list.Add(dazmorphFormula);
						}
					}
					else if (text3 == "center_point/x")
					{
						dazmorphFormula.targetType = DAZMorphFormulaTargetType.BoneCenterX;
						if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
						{
							dazmorphFormula.multiplier = -dazmorphFormula.multiplier * 0.01f;
							list.Add(dazmorphFormula);
						}
					}
					else if (text3 == "center_point/y")
					{
						dazmorphFormula.targetType = DAZMorphFormulaTargetType.BoneCenterY;
						if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
						{
							dazmorphFormula.multiplier *= 0.01f;
							list.Add(dazmorphFormula);
						}
					}
					else if (text3 == "center_point/z")
					{
						dazmorphFormula.targetType = DAZMorphFormulaTargetType.BoneCenterZ;
						if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
						{
							dazmorphFormula.multiplier *= 0.01f;
							list.Add(dazmorphFormula);
						}
					}
					else if (!Regex.IsMatch(text3, "^end_point"))
					{
						if (text3 == "orientation/x")
						{
							dazmorphFormula.targetType = DAZMorphFormulaTargetType.OrientationX;
							if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
							{
								list.Add(dazmorphFormula);
							}
						}
						else if (text3 == "orientation/y")
						{
							dazmorphFormula.targetType = DAZMorphFormulaTargetType.OrientationY;
							if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
							{
								dazmorphFormula.multiplier *= -1f;
								list.Add(dazmorphFormula);
							}
						}
						else if (text3 == "orientation/z")
						{
							dazmorphFormula.targetType = DAZMorphFormulaTargetType.OrientationZ;
							if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
							{
								dazmorphFormula.multiplier *= -1f;
								list.Add(dazmorphFormula);
							}
						}
						else if (text3 == "rotation/x")
						{
							dazmorphFormula.targetType = DAZMorphFormulaTargetType.RotationX;
							if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
							{
								list.Add(dazmorphFormula);
							}
						}
						else if (text3 == "rotation/y")
						{
							dazmorphFormula.targetType = DAZMorphFormulaTargetType.RotationY;
							if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
							{
								list.Add(dazmorphFormula);
							}
						}
						else if (text3 == "rotation/z")
						{
							dazmorphFormula.targetType = DAZMorphFormulaTargetType.RotationZ;
							if (this.ProcessFormula(jsonnode, dazmorphFormula, this.morphName))
							{
								list.Add(dazmorphFormula);
							}
						}
						else
						{
							Debug.LogWarning("Morph " + this.morphName + " has unknown output type " + text);
						}
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			this.formulas = list.ToArray();
		}
		else
		{
			this.formulas = new DAZMorphFormula[0];
		}
		this.numDeltas = mn["morph"]["deltas"]["count"].AsInt;
		this.deltas = new DAZMorphVertex[this.numDeltas];
		int num = 0;
		IEnumerator enumerator2 = mn["morph"]["deltas"]["values"].AsArray.GetEnumerator();
		try
		{
			while (enumerator2.MoveNext())
			{
				object obj2 = enumerator2.Current;
				JSONNode jsonnode2 = (JSONNode)obj2;
				int asInt = jsonnode2[0].AsInt;
				Vector3 delta;
				delta.x = -jsonnode2[1].AsFloat * 0.01f;
				delta.y = jsonnode2[2].AsFloat * 0.01f;
				delta.z = jsonnode2[3].AsFloat * 0.01f;
				DAZMorphVertex dazmorphVertex = new DAZMorphVertex();
				dazmorphVertex.vertex = asInt;
				dazmorphVertex.delta = delta;
				this.deltas[num] = dazmorphVertex;
				num++;
			}
		}
		finally
		{
			IDisposable disposable2;
			if ((disposable2 = (enumerator2 as IDisposable)) != null)
			{
				disposable2.Dispose();
			}
		}
	}

	public void LoadMeta()
	{
		if (this.metaJSONFile != null)
		{
			JSONNode jsonnode = DAZImport.ReadJSON(this.metaJSONFile);
			if (jsonnode != null)
			{
				this.LoadMetaFromJSON(jsonnode);
			}
		}
	}

	public void LoadMetaFromJSON(JSONNode metan)
	{
		this.morphName = metan["id"];
		this.displayName = metan["displayName"];
		this.overrideName = metan["id"];
		this.group = metan["group"];
		this.region = metan["region"];
		if (metan["min"] != null)
		{
			this.min = (float)metan["min"].AsInt;
		}
		if (metan["max"] != null)
		{
			this.max = (float)metan["max"].AsInt;
		}
		if (metan["numDeltas"] != null)
		{
			this.numDeltas = metan["numDeltas"].AsInt;
		}
		if (metan["isPoseControl"] != null)
		{
			this.isPoseControl = metan["isPoseControl"].AsBool;
		}
		if (metan["formulas"] != null)
		{
			List<DAZMorphFormula> list = new List<DAZMorphFormula>();
			IEnumerator enumerator = metan["formulas"].AsArray.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					JSONClass jsonclass = (JSONClass)obj;
					DAZMorphFormula dazmorphFormula = new DAZMorphFormula();
					try
					{
						bool flag = false;
						if (jsonclass["targetType"] != null)
						{
							dazmorphFormula.targetType = (DAZMorphFormulaTargetType)Enum.Parse(typeof(DAZMorphFormulaTargetType), jsonclass["targetType"]);
						}
						else
						{
							flag = true;
						}
						if (jsonclass["target"] != null)
						{
							dazmorphFormula.target = jsonclass["target"];
						}
						else
						{
							flag = true;
						}
						if (jsonclass["multiplier"] != null)
						{
							dazmorphFormula.multiplier = jsonclass["multiplier"].AsFloat;
						}
						else
						{
							flag = true;
						}
						if (!flag)
						{
							list.Add(dazmorphFormula);
						}
					}
					catch (ArgumentException)
					{
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			this.formulas = list.ToArray();
		}
	}

	public JSONClass GetMetaJSON()
	{
		JSONClass jsonclass = new JSONClass();
		if (this.morphName != null)
		{
			jsonclass["id"] = this.morphName;
		}
		if (this.displayName != null)
		{
			jsonclass["displayName"] = this.displayName;
		}
		if (this.overrideName != null)
		{
			jsonclass["overrideName"] = this.morphName;
		}
		if (this.group != null)
		{
			jsonclass["group"] = this.group;
		}
		if (this.region != null)
		{
			jsonclass["region"] = this.region;
		}
		jsonclass["min"].AsFloat = this.min;
		jsonclass["max"].AsFloat = this.max;
		jsonclass["numDeltas"].AsInt = this.numDeltas;
		jsonclass["isPoseControl"].AsBool = this.isPoseControl;
		JSONArray jsonarray = new JSONArray();
		foreach (DAZMorphFormula dazmorphFormula in this.formulas)
		{
			JSONClass jsonclass2 = new JSONClass();
			if (dazmorphFormula.targetType == DAZMorphFormulaTargetType.MCM || dazmorphFormula.targetType == DAZMorphFormulaTargetType.MCMMult)
			{
				Debug.LogError("Morph " + this.morphName + " will not be compiled because it is an MCM morph which can cause unwanted changes to shape");
				return null;
			}
			jsonclass2["targetType"] = dazmorphFormula.targetType.ToString();
			jsonclass2["target"] = dazmorphFormula.target;
			jsonclass2["multiplier"].AsFloat = dazmorphFormula.multiplier;
			jsonarray.Add(jsonclass2);
		}
		jsonclass["formulas"] = jsonarray;
		return jsonclass;
	}

	public void LoadDeltas()
	{
		if (this.deltasLoadPath != null && this.deltasLoadPath != string.Empty && !this.deltasLoaded)
		{
			this.deltasLoaded = true;
			this.LoadDeltasFromBinaryFile(this.deltasLoadPath);
		}
	}

	public void LoadDeltasFromJSON(JSONNode deltan)
	{
		JSONArray asArray = deltan["deltas"].AsArray;
		if (asArray != null)
		{
			this.numDeltas = asArray.Count;
			this.deltas = new DAZMorphVertex[this.numDeltas];
			int num = 0;
			IEnumerator enumerator = asArray.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					JSONNode jsonnode = (JSONNode)obj;
					DAZMorphVertex dazmorphVertex = new DAZMorphVertex();
					Vector3 delta;
					delta.x = jsonnode["x"].AsFloat;
					delta.y = jsonnode["y"].AsFloat;
					delta.z = jsonnode["z"].AsFloat;
					dazmorphVertex.delta = delta;
					dazmorphVertex.vertex = jsonnode["vid"].AsInt;
					this.deltas[num] = dazmorphVertex;
					num++;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}
	}

	public void LoadDeltasFromBinaryFile(string path)
	{
		try
		{
			using (BinaryReader binaryReader = new BinaryReader(File.Open(path, FileMode.Open)))
			{
				this.numDeltas = binaryReader.ReadInt32();
				this.deltas = new DAZMorphVertex[this.numDeltas];
				for (int i = 0; i < this.numDeltas; i++)
				{
					DAZMorphVertex dazmorphVertex = new DAZMorphVertex();
					dazmorphVertex.vertex = binaryReader.ReadInt32();
					Vector3 delta;
					delta.x = binaryReader.ReadSingle();
					delta.y = binaryReader.ReadSingle();
					delta.z = binaryReader.ReadSingle();
					dazmorphVertex.delta = delta;
					this.deltas[i] = dazmorphVertex;
				}
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(string.Concat(new object[]
			{
				"Error while loading binary delta file ",
				path,
				" ",
				ex
			}));
		}
	}

	public void SaveDeltasToBinaryFile(string path)
	{
		using (BinaryWriter binaryWriter = new BinaryWriter(File.Open(path, FileMode.Create)))
		{
			binaryWriter.Write(this.numDeltas);
			foreach (DAZMorphVertex dazmorphVertex in this.deltas)
			{
				binaryWriter.Write(dazmorphVertex.vertex);
				binaryWriter.Write(dazmorphVertex.delta.x);
				binaryWriter.Write(dazmorphVertex.delta.y);
				binaryWriter.Write(dazmorphVertex.delta.z);
			}
		}
	}

	public JSONClass GetDeltasJSON()
	{
		JSONClass jsonclass = new JSONClass();
		JSONArray jsonarray = new JSONArray();
		foreach (DAZMorphVertex dazmorphVertex in this.deltas)
		{
			JSONClass jsonclass2 = new JSONClass();
			jsonclass2["x"].AsFloat = dazmorphVertex.delta.x;
			jsonclass2["y"].AsFloat = dazmorphVertex.delta.y;
			jsonclass2["z"].AsFloat = dazmorphVertex.delta.z;
			jsonclass2["vid"].AsInt = dazmorphVertex.vertex;
			jsonarray.Add(jsonclass2);
		}
		jsonclass["deltas"] = jsonarray;
		return jsonclass;
	}

	private const float geoScale = 0.01f;

	public bool visible;

	public bool preserveValueOnReimport;

	public bool disable;

	public bool isPoseControl;

	public string morphName;

	public string displayName;

	public string overrideName;

	public string region;

	public string group;

	public float importValue;

	public float startValue;

	[SerializeField]
	private float _morphValue;

	public float appliedValue;

	public float min;

	public float max;

	public int numDeltas;

	public bool triggerNormalRecalc = true;

	public bool triggerTangentRecalc = true;

	public DAZMorphVertex[] deltas;

	public DAZMorphFormula[] formulas;

	[NonSerialized]
	public JSONStorableFloat jsonFloat;

	public DAZMorph.SetAnimatableCallback setAnimatableCallback;

	protected Toggle _animatableToggle;

	protected Toggle _animatableToggleAlt;

	protected bool _startingAnimatable;

	[SerializeField]
	protected bool _animatable;

	public bool isTransient;

	public bool isRuntime;

	public string metaLoadPath = string.Empty;

	protected Button _increaseRangeButton;

	protected Button _increaseRangeButtonAlt;

	protected Button _resetRangeButton;

	protected Button _resetRangeButtonAlt;

	public string metaJSONFile;

	public bool deltasLoaded;

	public string deltasLoadPath = string.Empty;

	public delegate void SetAnimatableCallback(DAZMorph dm);
}
